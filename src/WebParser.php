<?php declare(strict_types=1);

namespace DromParser;

use DromParser\Result\ResultInterface;
use DromParser\Utils\ProxyKeeper;
use DromParser\Utils\ResourceReader;
use DromParser\WebParser\Filter\Exceptions\InvalidFilterParamsException;
use DromParser\WebParser\Filter\FilterAuto;
use DromParser\WebParser\WebParserAuto;
use JsonException;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

readonly class WebParser
{
    public function __construct(
        private ?ProxyKeeper $proxyKeeper = null,
        private LoggerInterface $logger = new NullLogger()
    ) {}

    /**
     * @return ResultInterface[]
     * @throws JsonException
     * @throws InvalidFilterParamsException
     */
    public function parseAuto(FilterAuto $filterAuto = new FilterAuto()): array
    {
        $resourceReader = new ResourceReader($this->proxyKeeper, $this->logger);
        $webParserAuto = new WebParserAuto($filterAuto, $resourceReader, $this->logger);
        $webParserAuto->run();

        return $webParserAuto->getResult();
    }
}
