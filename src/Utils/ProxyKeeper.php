<?php declare(strict_types=1);

namespace DromParser\Utils;

use JsonException;

/**
 * Хранит список прокси серверов
 */
class ProxyKeeper
{
    private int $index;

    /**
     * @param string[] $proxyList
     */
    public function __construct(private readonly array $proxyList = [])
    {
        if (!empty($proxyList)) {
            $this->index = count($this->proxyList) - 1;
        }
    }

    /**
     * Возвращет следующий в списке прокси сервер, если список закончился, начинает с начала
     */
    public function getNextProxy(): ?string
    {
        if (empty($this->proxyList)) {
            return null;
        }

        if ($this->index === count($this->proxyList) - 1) {
            $this->index = 0;
        } else {
            $this->index++;
        }

        return $this->proxyList[$this->index];
    }

    /**
     * Получение списка прокси серверов из файла в формате json
     *
     * @throws JsonException
     */
    public static function createFromFile(string $path): self
    {
        $json = file_get_contents($path);

        return new self(
            json_decode($json, true, JSON_THROW_ON_ERROR)
        );
    }
}
