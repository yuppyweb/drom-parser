<?php declare(strict_types=1);

namespace DromParser\Utils;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Throwable;

readonly class ResourceReader
{
    private const ATTEMPTS_GET_CONTENT = 5;

    public function __construct(
        private ?ProxyKeeper $proxyKeeper = null,
        private LoggerInterface $logger = new NullLogger()
    ) {}

    /**
     * Метод для получения содержимого ресурса с использованием прокси
     */
    public function getContent(string $resource): ?string
    {
        $this->logger->info("Getting the content of a resource $resource");

        for ($i = 0; $i < self::ATTEMPTS_GET_CONTENT; $i++) {
            try {
                $content = file_get_contents($resource, false, $this->getStreamContext());

                if ($content !== false) {
                    return $content;
                }

                $this->logger->warning("Failed to get content, attempt " . ($i + 1));
            } catch (Throwable $t) {
                $this->logger->error(
                    "Error at " . ($i + 1) . " attempt to get content: {$t->getMessage()}"
                );
            }
        }

        return null;
    }

    /**
     * @return resource|null
     */
    private function getStreamContext()
    {
        if (empty($this->proxyKeeper)) {
            return null;
        }

        $proxy = $this->proxyKeeper->getNextProxy();

        if (empty($proxy)) {
            return null;
        }

        $this->logger->info("Proxy $proxy will be used");

        return stream_context_create([
            'http' => [
                'proxy' => $proxy,
                'request_fulluri' => true,
            ]
        ]);
    }
}
