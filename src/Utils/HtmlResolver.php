<?php declare(strict_types=1);

namespace DromParser\Utils;

use DOMDocument;
use DOMNodeList;
use DOMXPath;

readonly class HtmlResolver
{
    private DOMDocument $document;

    private DOMXPath $xpath;

    public function __construct(string $html)
    {
        $this->document = new DOMDocument();
        $this->document->loadHTML($html, LIBXML_NOERROR);
        $this->xpath = new DOMXPath($this->document);
    }

    /**
     * Получает элемент по xpath
     */
    public function getByXpath(string $xpath): ?DOMNodeList
    {
        $DOMNodeList = $this->xpath->query($xpath);

        if (!($DOMNodeList instanceof DOMNodeList)) {
            return null;
        }

        return $DOMNodeList;
    }

    public static function load(string $html): self
    {
        return new self($html);
    }
}
