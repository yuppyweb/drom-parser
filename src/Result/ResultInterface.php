<?php declare(strict_types=1);

namespace DromParser\Result;

interface ResultInterface
{
    public function toArray(): array;
}
