<?php declare(strict_types=1);

namespace DromParser\Result;

/**
 * Результат разбора объявления с автомобилем
 */
class ResultAuto implements ResultInterface
{
    private string $brand;

    private string $model;

    private int $price;

    private int $dromPrice;

    private string $dealType;

    private string $generation;

    private string $complectation;

    private string $mileage;

    private string $mileageInRussia;

    private string $color;

    private string $frameType;

    private string $power;

    private string $fuel;

    private string $volume;

    private string $isHybrid;

    private string $isGbo;

    /**
     * @var string[]
     */
    private array $photoUrls;

    public function __construct(
        public readonly int $dromId,
        public readonly string $url
    ) {}

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function setDromPrice(int $dromPrice): self
    {
        $this->dromPrice = $dromPrice;

        return $this;
    }

    public function setDealType(string $dealType): self
    {
        $this->dealType = $dealType;

        return $this;
    }

    public function setGeneration(string $generation): self
    {
        $this->generation = $generation;

        return $this;
    }

    public function setComplectation(string $complectation): self
    {
        $this->complectation = $complectation;

        return $this;
    }

    public function setMileage(string $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function setMileageInRussia(string $mileageInRussia): self
    {
        $this->mileageInRussia = $mileageInRussia;

        return $this;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function setFrameType(string $frameType): self
    {
        $this->frameType = $frameType;

        return $this;
    }

    public function setPower(string $power): self
    {
        $this->power = $power;

        return $this;
    }

    public function setFuel(string $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function setVolume(string $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function setIsHybrid(string $isHybrid): self
    {
        $this->isHybrid = $isHybrid;

        return $this;
    }

    public function setIsGbo(string $isGbo): self
    {
        $this->isGbo = $isGbo;

        return $this;
    }

    /**
     * @param string[] $photoUrls
     */
    public function setPhotoUrls(array $photoUrls): self
    {
        $this->photoUrls = $photoUrls;

        return $this;
    }

    /**
     * @return array{
     *     dromId: int,
     *     url: string,
     *     brand: string|null,
     *     model: string|null,
     *     price: int|null,
     *     dromPrice: int|null,
     *     dealType: string|null,
     *     generation: string|null,
     *     complectation: string|null,
     *     mileage: string|null,
     *     mileageInRussia: string|null,
     *     color: string|null,
     *     frameType: string|null,
     *     power: string|null,
     *     fuel: string|null,
     *     volume: string|null,
     *     isHybrid: string|null,
     *     isGbo: string|null,
     *     photoUrls: array|null
     * }
     */
    public function toArray(): array
    {
        return [
            'dromId' => $this->dromId,
            'url' => $this->url,
            'brand' => $this->brand ?? null,
            'model' => $this->model ?? null,
            'price' => $this->price ?? null,
            'dromPrice' => $this->dromPrice ?? null,
            'dealType' => $this->dealType ?? null,
            'generation' => $this->generation ?? null,
            'complectation' => $this->complectation ?? null,
            'mileage' => $this->mileage ?? null,
            'mileageInRussia' => $this->mileageInRussia ?? null,
            'color' => $this->color ?? null,
            'frameType' => $this->frameType ?? null,
            'power' => $this->power ?? null,
            'fuel' => $this->fuel ?? null,
            'volume' => $this->volume ?? null,
            'isHybrid' => $this->isHybrid ?? null,
            'isGbo' => $this->isGbo ?? null,
            'photoUrls' => $this->photoUrls ?? null,
        ];
    }
}
