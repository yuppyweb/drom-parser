<?php declare(strict_types=1);

namespace DromParser\WebParser;

use DromParser\Result\ResultInterface;

interface WebParserInterface
{
    public function run(): void;

    /**
     * @return ResultInterface[]
     */
    public function getResult(): array;
}
