<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter\Exceptions;

use Exception;

class InvalidFilterParamsException extends Exception
{

}
