<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter;

use DromParser\WebParser\Filter\Exceptions\InvalidFilterParamsException;
use DromParser\WebParser\Filter\Types\Auto;
use DromParser\WebParser\Filter\Types\City;
use DromParser\WebParser\Filter\Types\Damage;
use DromParser\WebParser\Filter\Types\Documents;
use DromParser\WebParser\Filter\Types\Region;

/**
 * Фильтр по объявлениям с автомобилями
 */
class FilterAuto implements FilterInterface
{
    /**
     * @var Auto[]
     */
    private array $autos = [];

    /**
     * @var Region[]
     */
    private array $regions = [];

    /**
     * @var City[]
     */
    private array $cities = [];

    private Documents $documents;

    private Damage $damage;

    private int $unsold;

    public function __construct()
    {
    }

    /**
     * @return array{
     *     rid?: array,
     *     cid?: array,
     *     pts?: int,
     *     damaged?: int,
     *     unsold?: int,
     *     multiselect?: array
     * }
     * @throws InvalidFilterParamsException
     */
    public function getFilter(): array
    {
        $filter = [];

        if (!empty($this->regions)) {
            $filter['rid'] = array_map(
                fn (Region $region) => $region->value,
                $this->regions
            );

            $filter['rid'] = array_values(array_unique($filter['rid']));
        }

        if (!empty($this->cities)) {
            $filter['cid'] = array_map(
                fn (City $city) => $city->value,
                $this->cities
            );

            $filter['cid'] = array_values(array_unique($filter['cid']));
        }

        if (!empty($this->documents)) {
            $filter['pts'] = $this->documents->value;
        }

        if (!empty($this->damage)) {
            $filter['damaged'] = $this->damage->value;
        }

        if (!empty($this->unsold)) {
            $filter['unsold'] = $this->unsold;
        }

        if (!empty($this->autos)) {
            $filter['multiselect'] = $this->getMultiselect();
        }

        return $filter;
    }

    /**
     * Фильтр по автомобилям
     */
    public function setAutos(Auto ...$autos): self
    {
        $this->autos = $autos;

        return $this;
    }

    /**
     * Фильтр по регионам
     */
    public function setRegions(Region ...$regions): self
    {
        $this->regions = $regions;

        return $this;
    }

    /**
     * Фильтр по городам
     */
    public function setCities(City ...$cities): self
    {
        $this->cities = $cities;

        return $this;
    }

    /**
     * Фильтр по документам (впорядке или проблемные)
     */
    public function setDocuments(Documents $documents): self
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * Фильтр по повреждениям (требуется ремонт или нет)
     */
    public function setDamage(Damage $damage): self
    {
        $this->damage = $damage;

        return $this;
    }

    /**
     * Фильтр по непроданным
     */
    public function setUnsold(): self
    {
        $this->unsold = 1;

        return $this;
    }

    /**
     * Возвращает массив строк, в каждой строке числа с разделителем "_", например 7_119_3_1
     * первое число - id марки
     * второе число - id модели
     * третье число - номер поколения
     * четвертое число - номер рестайлинга (0 - дорестайлинг, 1 - первый рестайлинг и т.д.)
     *
     * @return string[]
     * @throws InvalidFilterParamsException
     */
    private function getMultiselect(): array
    {
        $multiselect = [];

        foreach ($this->autos as $auto) {
            $multiselectValue = [$auto->brand->value];

            if (!empty($auto->model)) {
                if ($auto->brand !== $auto->model->getBrand()) {
                    throw new InvalidFilterParamsException(
                        "Incorrect model passed for {$auto->brand->name} brand: {$auto->model->name}"
                    );
                }

                $multiselectValue[] = $auto->model->value;

                if (!empty($auto->generation)) {
                    $generations = $auto->model->getGeneration();

                    if (empty($generations[$auto->generation])) {
                        throw new InvalidFilterParamsException(
                            "For the {$auto->brand->name} {$auto->model->name} model, " .
                            "the wrong generation was passed: {$auto->generation}"
                        );
                    }

                    $multiselectValue[] = $auto->generation;

                    if (!is_null($auto->restyle)) {
                        if (!in_array($auto->restyle, $generations[$auto->generation])) {
                            throw new InvalidFilterParamsException(
                                "For the {$auto->brand->name} {$auto->model->name} " .
                                "model in generation {$auto->generation}, an incorrect restyling value " .
                                "was passed: {$auto->restyle}"
                            );
                        }

                        $multiselectValue[] = $auto->restyle;
                    }
                }
            }

            $multiselect[] = implode('_', $multiselectValue);
        }

        return $multiselect;
    }
}
