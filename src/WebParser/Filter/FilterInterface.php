<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter;

interface FilterInterface
{
    public function getFilter(): array;
}
