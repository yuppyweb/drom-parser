<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter\Types;

/**
 * Перечисление повреждений для фильтра
 */
enum Damage: int
{
    case DAMAGED = 1;
    case UNDAMAGED = 2;
}
