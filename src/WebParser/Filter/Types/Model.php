<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter\Types;

/**
 * Перечисление моделей автомобилей для фильтра
 */
enum Model: int
{
    case CROWN = 4;
    case MARK2 = 7;
    case LEGACY = 119;
    case FORESTER = 245;

    /**
     * Получение марки автомобиля для текущей модели
     */
    public function getBrand(): Brand
    {
        return match ($this) {
            self::FORESTER,
            self::LEGACY => Brand::SUBARU,
            self::CROWN,
            self::MARK2 => Brand::TOYOTA,
        };
    }

    /**
     * Получение всех поколений и рестайлингов для текущей модели
     *
     * @return int[][]
     */
    public function getGeneration(): array
    {
        return match ($this) {
            self::FORESTER => [
                1 => [0, 1],
                2 => [0, 1],
                3 => [0, 1],
                4 => [0, 1],
                5 => [0, 1]
            ],
            self::LEGACY => [
                1 => [0, 1],
                2 => [0, 1],
                3 => [0, 1],
                4 => [0, 1],
                5 => [0, 1],
                6 => [0, 1],
                7 => [0],
                8 => [1]
            ],
            self::CROWN => [
                1 => [0],
                2 => [0],
                3 => [0],
                4 => [0],
                5 => [0],
                6 => [0],
                7 => [0],
                8 => [0, 1, 2],
                9 => [0, 1],
                10 => [0, 1],
                11 => [0, 1],
                12 => [0, 1],
                13 => [0, 1],
                14 => [0, 1],
                15 => [0],
                16 => [0],
            ],
            self::MARK2 => [
                1 => [0],
                2 => [0],
                3 => [0],
                4 => [0, 1],
                5 => [0, 1, 2],
                6 => [0, 1],
                7 => [0, 1],
                8 => [0, 1],
                9 => [0, 1],
            ],
        };
    }
}
