<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter\Types;

/**
 * Перечисление регионов для фильтра
 */
enum Region: int
{
    case PRIMORSKII_KRAI = 25;
    case OMSKAIA_OBL = 55;
}
