<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter\Types;

/**
 * Перечисление в каком состоянии документы для фильтра
 */
enum Documents: int
{
    case PROBLEM = 1;
    case OK = 2;
}
