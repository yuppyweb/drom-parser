<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter\Types;

/**
 * Перечисление марок автомобилей для фильтра
 */
enum Brand: int
{
    case SUBARU = 7;
    case TOYOTA = 9;
}
