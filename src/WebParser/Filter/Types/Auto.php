<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter\Types;

/**
 * Автомобиль для фильтра
 */
readonly class Auto
{
    public function __construct(
        public Brand $brand,
        public ?Model $model = null,
        public ?int $generation = null,
        public ?int $restyle = null
    ) {}
}
