<?php declare(strict_types=1);

namespace DromParser\WebParser\Filter\Types;

/**
 * Перечисление городов для фильтра
 */
enum City: int
{
    case VLADIVOSTOK = 23;
    case NAKHODKA = 98;
    case USSURIISK = 170;
}
