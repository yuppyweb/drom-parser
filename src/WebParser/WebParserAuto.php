<?php declare(strict_types=1);

namespace DromParser\WebParser;

use DOMElement;
use DromParser\Result\ResultAuto;
use DromParser\Result\ResultInterface;
use DromParser\Utils\HtmlResolver;
use DromParser\Utils\ResourceReader;
use DromParser\WebParser\Filter\Exceptions\InvalidFilterParamsException;
use DromParser\WebParser\Filter\FilterAuto;
use JsonException;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Разбирает объявления с автомобилями
 */
class WebParserAuto implements WebParserInterface
{
    private const BASE_URL = 'https://auto.drom.ru/';
    private const ATTRIBUTE_DROM_MODULE_DATA = 'data-drom-module-data';

    /**
     * @var ResultAuto[]
     */
    private array $autoList;

    public function __construct(
        private readonly FilterAuto $filterAuto,
        private readonly ResourceReader $resourceReader,
        private readonly LoggerInterface $logger = new NullLogger()
    ) {}

    /**
     * Запускает парсинг объявлений
     *
     * @throws JsonException
     * @throws InvalidFilterParamsException
     */
    public function run(): void
    {
        $this->logger->info("Start parsing");

        $this->findAuto();
        $this->parseAutoList();

        $this->logger->info("End parsing");
    }

    /**
     * Возвращает результат разбора объявлений
     *
     * @return ResultInterface[]
     */
    public function getResult(): array
    {
        return $this->autoList;
    }

    /**
     * Поиск объявлений с учетом фильтра
     *
     * @throws JsonException
     * @throws InvalidFilterParamsException
     */
    private function findAuto(): void
    {
        $this->autoList = [];
        $pageUrl = $this->getFirstPageUrl();

        while (!empty($pageUrl)) {
            $dromListData = $this->getDromListData($pageUrl);

            if (empty($dromListData)) {
                break;
            }

            $autoListOnPage = $this->getAutoListOnPage($dromListData);
            $this->logger->info("Ads found on the page: " . count($autoListOnPage));

            $this->autoList = [...$this->autoList, ...$autoListOnPage];
            $pageUrl = $this->getPageUrl($dromListData);
        }

        $this->logger->info("Total ads found: " . count($this->autoList));
    }

    /**
     * @throws JsonException
     */
    private function getDromListData(string $url): array
    {
        $dromData = $this->getDromData($url);

        if (empty($dromData['bullList']['bullsData'][0])) {
            return [];
        }

        return $dromData['bullList']['bullsData'][0];
    }

    /**
     * Находим на странице елемент в котором находятся данные по объявлениям в формате json
     * и возвращаем их в виде массива
     *
     * @throws JsonException
     */
    private function getDromData(string $url): array
    {
        // Получаем html код страницы
        $html = $this->resourceReader->getContent($url);

        if (empty($html)) {
            return [];
        }

        $htmlResolver = HtmlResolver::load($html);

        // Ищем в коде элемент с данными по объявлениям
        $DOMNodeList = $htmlResolver->getByXpath(
            '//script[@' . self::ATTRIBUTE_DROM_MODULE_DATA . ']'
        );

        if (empty($DOMNodeList) || $DOMNodeList->count() === 0) {
            $this->logger->warning(
                "Selector with " . self::ATTRIBUTE_DROM_MODULE_DATA . " attribute not found"
            );

            return [];
        }

        /** @var DOMElement $DOMElement */
        $DOMElement = $DOMNodeList->item(0);

        if (empty($DOMElement)) {
            return [];
        }

        $dataJson = $DOMElement->getAttribute(self::ATTRIBUTE_DROM_MODULE_DATA);

        try {
            return json_decode($dataJson, true, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->logger->error("Failed to parse json: {$e->getMessage()}");

            return [];
        }
    }

    /**
     * Возвращает первую страницу со списком объявлений полученную с помощью фильтра
     *
     * @throws InvalidFilterParamsException
     */
    private function getFirstPageUrl(): string
    {
        $filter = $this->filterAuto->getFilter();

        if (empty($filter)) {
            return self::BASE_URL;
        }

        return self::BASE_URL . '?' . http_build_query($filter);
    }

    /**
     * Возвращаем урл следующей страницы постраничной навигации
     */
    private function getPageUrl(array $dromListData): ?string
    {
        if (empty($dromListData['pagination'])) {
            return null;
        }

        $pagination = $dromListData['pagination'];
        $lastPage = ceil($pagination['total'] / $pagination['itemsPerPage']);

        if ($pagination['page'] >= $lastPage) {
            return null;
        }

        return str_replace(
            '[page]',
            (string)($pagination['page'] + 1),
            $pagination['urlPattern']
        );
    }

    /**
     * Получаем список объявлений на странице
     *
     * @return ResultAuto[]
     */
    private function getAutoListOnPage($dromListData): array
    {
        if (empty($dromListData['bulls'])) {
            return [];
        }

        $resultAutoList = [];

        foreach ($dromListData['bulls'] as $bull) {
            $resultAutoList[] = new ResultAuto($bull['bullId'], $bull['url']);
        }

        return $resultAutoList;
    }

    /**
     * Разбирает все полученные объявления
     *
     * @throws JsonException
     */
    private function parseAutoList(): void
    {
        if (empty($this->autoList)) {
            return;
        }

        foreach ($this->autoList as $auto) {
            $this->parseAuto($auto);
        }
    }

    /**
     * Разбирает объявление
     *
     * @throws JsonException
     */
    private function parseAuto(ResultAuto $resultAuto): void
    {
        $dromData = $this->getDromData($resultAuto->url);

        $this->logger->info("Parsing the ad with ID {$resultAuto->dromId}");

        if (!empty($dromData['header']['breadcrumbs'][2]['name'])) {
            // Из хлебных крошек получаем название марки автомобиля
            $resultAuto->setBrand($dromData['header']['breadcrumbs'][2]['name']);
            $this->logger->info("Brand: {$dromData['header']['breadcrumbs'][2]['name']}");
        }

        if (!empty($dromData['header']['breadcrumbs'][3]['name'])) {
            // Из хлебных крошек получаем название модели автомобиля
            $resultAuto->setModel($dromData['header']['breadcrumbs'][3]['name']);
            $this->logger->info("Model: {$dromData['header']['breadcrumbs'][3]['name']}");
        }

        if (!empty($dromData['constants']['price'])) {
            $resultAuto->setPrice($dromData['constants']['price']);
            $this->logger->info("Price: {$dromData['constants']['price']}");
        }

        if (!empty($dromData['priceWidgetData']['goodDealConfig']['marketPrice'])) {
            $resultAuto->setDromPrice(
                $dromData['priceWidgetData']['goodDealConfig']['marketPrice']
            );
            $this->logger->info(
                "Drom price: {$dromData['priceWidgetData']['goodDealConfig']['marketPrice']}"
            );
        }

        if (!empty($dromData['priceWidgetData']['goodDealConfig']['dealType'])) {
            $dealTypeText = $this->getDealTypeText(
                $dromData['priceWidgetData']['goodDealConfig']['dealType']
            );
            $resultAuto->setDealType($dealTypeText);
            $this->logger->info("Deal type: $dealTypeText");
        }

        $generationPayload = $this->getDescriptionByType('generation', $dromData);
        $complectationPayload = $this->getDescriptionByType('complectation', $dromData);
        $mileagePayload = $this->getDescriptionByType('mileage', $dromData);
        $colorPayload = $this->getDescriptionByType('color', $dromData);
        $frameTypePayload = $this->getDescriptionByType('frameType', $dromData);
        $powerPayload = $this->getDescriptionByType('power', $dromData);
        $enginePayload = $this->getDescriptionByType('engine', $dromData);

        if (!empty($generationPayload['generationName'])) {
            $resultAuto->setGeneration($generationPayload['generationName']);
            $this->logger->info("Generation: {$generationPayload['generationName']}");
        }

        if (!empty($complectationPayload['complectationName'])) {
            $resultAuto->setComplectation($complectationPayload['complectationName']);
            $this->logger->info("Complectation: {$complectationPayload['complectationName']}");
        }

        if (!empty($mileagePayload['mileage']) && !empty($mileagePayload['units'])) {
            $resultAuto->setMileage("{$mileagePayload['mileage']} {$mileagePayload['units']}");
            $this->logger->info("Mileage: {$mileagePayload['mileage']} {$mileagePayload['units']}");
        }

        if (!empty($mileagePayload['withoutMileageInRussia'])) {
            $textMileageInRussia = 'без пробега по РФ';
            $resultAuto->setMileageInRussia($textMileageInRussia);
            $this->logger->info("Without mileage in Russia: $textMileageInRussia");
        }

        if (!empty($colorPayload['color'])) {
            $colorText = $this->getColorText($colorPayload['color']);
            $resultAuto->setColor($colorText);
            $this->logger->info("Color: $colorText");
        }

        if (!empty($frameTypePayload)) {
            $frameTypeText = $this->getFrameTypeText($frameTypePayload);
            $resultAuto->setFrameType($frameTypeText);
            $this->logger->info("Frame type: $frameTypeText");
        }

        if (!empty($powerPayload['power'])) {
            $powerText = "{$powerPayload['power']} л.с.";
            $resultAuto->setPower($powerText);
            $this->logger->info("Power: $powerText");
        }

        if (!empty($enginePayload['fuel'])) {
            $fuelText = $this->getFuelText($enginePayload['fuel']);
            $resultAuto->setFuel($fuelText);
            $this->logger->info("Fuel: $fuelText");
        }

        if (!empty($enginePayload['volume'])) {
            $volumeText = "{$enginePayload['volume']} л";
            $resultAuto->setVolume($volumeText);
            $this->logger->info("Volume: $volumeText");
        }

        if (!empty($enginePayload['isHybrid'])) {
            $isHybridText = "гибрид";
            $resultAuto->setIsHybrid($isHybridText);
            $this->logger->info("Is hybrid: $isHybridText");
        }

        if (!empty($enginePayload['isGbo'])) {
            $isGboText = "ГБО";
            $resultAuto->setIsGbo($isGboText);
            $this->logger->info("Is GBO: $isGboText");
        }

        if (!empty($dromData['gallery']['photos'])) {
            $photoUrls = [];

            foreach ($dromData['gallery']['photos'] as $photo) {
                $photoUrls[] = $photo['original'];
                $this->logger->info("Photo URL: {$photo['original']}");
            }

            $resultAuto->setPhotoUrls($photoUrls);
        }

        $this->logger->info("The ad with ID {$resultAuto->dromId} has been parsed");
    }

    private function getDealTypeText(int $dealType): string
    {
        return match ($dealType) {
            2 => 'высокая цена',
            3 => 'нормальная цена',
            4 => 'хорошая цена',
            5 => 'отличная цена',
            default => 'без оценки',
        };
    }

    private function getDescriptionByType(string $type, array $dromData): mixed
    {
        if (empty($dromData['bullDescription']['fields'])) {
            return null;
        }

        foreach ($dromData['bullDescription']['fields'] as $field) {
            if ($field['type'] !== $type) {
                continue;
            }

            return $field['payload'] ?? null;
        }

        return null;
    }

    private function getColorText(int $colorId): ?string
    {
        return match ($colorId) {
            1 => 'черный',
            2 => 'фиолетовый',
            3 => 'синий',
            4 => 'серый',
            5 => 'оранжевый',
            6 => 'красный',
            7 => 'коричневый',
            8 => 'золотистый',
            9 => 'зеленый',
            10 => 'желтый',
            11 => 'бордовый',
            12 => 'белый',
            13 => 'бежевый',
            14 => 'голубой',
            15 => 'розовый',
            16 => 'серебристый',
            default => 'другой',
        };
    }

    private function getFrameTypeText($frameTypeId): string
    {
        return match ($frameTypeId) {
            1 => 'Купе',
            2 => 'Грузовик',
            3 => 'Универсал',
            4 => 'Хэтчбек 3 дв.',
            5 => 'Хэтчбек 5 дв.',
            6 => 'Минивэн',
            7 => 'Джип 5 дв.',
            8 => 'Джип 3 дв.',
            9 => 'Лифтбек',
            10 => 'Седан',
            11 => 'Открытый',
            12 => 'Пикап',
            default => 'Другой',
        };
    }

    private function getFuelText($fuelId): string
    {
        return match ($fuelId) {
            1 => 'бензин',
            2 => 'дизель',
            4 => 'электро',
            default => 'другой',
        };
    }
}
