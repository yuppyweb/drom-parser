## How to add to a project

```
composer config repositories.yuppyweb git https://gitlab.com/yuppyweb/drom-parser.git
```
```
composer require yuppyweb/drom-parser:dev-main
```

## How to use

```
require_once 'vendor/autoload.php';

use DromParser\Utils\ProxyKeeper;
use DromParser\WebParser;
use DromParser\WebParser\Filter\FilterAuto;
use DromParser\WebParser\Filter\Types\Auto;
use DromParser\WebParser\Filter\Types\Brand;
use DromParser\WebParser\Filter\Types\City;
use DromParser\WebParser\Filter\Types\Damage;
use DromParser\WebParser\Filter\Types\Documents;
use DromParser\WebParser\Filter\Types\Model;
    
try {
    $filterAuto = (new FilterAuto())
        ->setAutos(
            new Auto(Brand::TOYOTA, Model::CROWN, 15),
            new Auto(Brand::TOYOTA, Model::CROWN, 16),
        )
        ->setCities(City::VLADIVOSTOK, City::USSURIISK)
        ->setDamage(Damage::UNDAMAGED)
        ->setDocuments(Documents::OK)
        ->setUnsold();
    
    $proxyKeeper = new ProxyKeeper([
        "tcp://185.174.138.19:80",
        "tcp://45.8.211.90:80",
        "tcp://31.12.75.183:80",
    ]);
    
    $webParser = new WebParser($proxyKeeper);
    $resultAutos = $webParser->parseAuto($filterAuto);
    
    foreach ($resultAutos as $resultAuto) {
        print_r($resultAuto->toArray());
    }
} catch (Throwable $t) {
    print("Parsing error: {$t->getMessage()}");
}
```

## Run tests

```
composer test
```
