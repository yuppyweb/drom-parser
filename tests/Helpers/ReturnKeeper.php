<?php declare(strict_types=1);

namespace DromParser\Test\Helpers;

class ReturnKeeper
{
    private int $index = 0;

    public function __construct(
        private array $returns = []
    ) {}

    public function addReturn(mixed $return): self
    {
        $this->returns[] = $return;

        return $this;
    }

    public function getReturn(): mixed
    {
        if (empty($this->returns)) {
            return null;
        }

        return $this->returns[$this->index++] ?? null;
    }
}
