<?php declare(strict_types=1);

namespace DromParser\Test\Helpers;

use DromParser\Utils\ResourceReader;

readonly class MockResourceReader extends ResourceReader
{
    public function __construct(private ReturnKeeper $returnKeeper)
    {
        parent::__construct();
    }

    public function getContent(string $resource): ?string
    {
        $path = $this->returnKeeper->getReturn();

        if (empty($path)) {
            return null;
        }

        $content = file_get_contents($path);

        return $content ?: null;
    }
}
