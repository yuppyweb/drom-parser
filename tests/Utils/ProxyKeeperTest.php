<?php declare(strict_types=1);

namespace DromParser\Test\Utils;

use DromParser\Utils\ProxyKeeper;
use PHPUnit\Framework\TestCase;

class ProxyKeeperTest extends TestCase
{
    /**
     * @dataProvider getNextProxyDataProvider
     */
    public function testGetNextProxy(array $proxyList, int $countExecutions, ?string $expectedResult): void
    {
        $proxyKeeper = new ProxyKeeper($proxyList);
        $actualResult = null;

        for ($i = 0; $i < $countExecutions; $i++) {
            $actualResult = $proxyKeeper->getNextProxy();
        }

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testCreateFromFile(): void
    {
        $proxyKeeper = ProxyKeeper::createFromFile(__DIR__ . '/fixtures/proxy.json');

        $this->assertInstanceOf(ProxyKeeper::class, $proxyKeeper);
        $this->assertEquals('tcp://192.194.0.10', $proxyKeeper->getNextProxy());
        $this->assertEquals('tcp://192.194.10.1', $proxyKeeper->getNextProxy());
        $this->assertEquals('tcp://192.194.0.10', $proxyKeeper->getNextProxy());
    }

    public static function getNextProxyDataProvider(): array
    {
        return [
            [
                [],
                1,
                null
            ],
            [
                ['tcp://192.194.10.1'],
                1,
                'tcp://192.194.10.1'
            ],
            [
                ['tcp://192.194.10.1'],
                2,
                'tcp://192.194.10.1'
            ],
            [
                [
                    'tcp://192.194.0.10',
                    'tcp://192.194.10.1',
                ],
                1,
                'tcp://192.194.0.10'
            ],
            [
                [
                    'tcp://192.194.0.10',
                    'tcp://192.194.10.1',
                ],
                2,
                'tcp://192.194.10.1'
            ],
            [
                [
                    'tcp://192.194.0.10',
                    'tcp://192.194.3.15',
                    'tcp://192.194.60.123',
                    'tcp://192.194.14.21',
                    'tcp://192.194.10.1',
                ],
                5,
                'tcp://192.194.10.1'
            ],
            [
                [
                    'tcp://192.194.0.10',
                    'tcp://192.194.3.15',
                    'tcp://192.194.60.123',
                    'tcp://192.194.14.21',
                    'tcp://192.194.10.1',
                ],
                6,
                'tcp://192.194.0.10'
            ],
        ];
    }
}
