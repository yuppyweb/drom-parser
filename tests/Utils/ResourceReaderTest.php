<?php declare(strict_types=1);

namespace DromParser\Test\Utils;

use DromParser\Utils\ResourceReader;
use PHPUnit\Framework\TestCase;

class ResourceReaderTest extends TestCase
{
    public function testGetContent(): void
    {
        $resourceReader = new ResourceReader();

        $this->assertStringContainsString(
            'drom',
            $resourceReader->getContent(__DIR__ . '/fixtures/index.html')
        );
    }
}
