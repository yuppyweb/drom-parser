<?php declare(strict_types=1);

namespace DromParser\Test\Utils;

use DOMNodeList;
use DromParser\Utils\HtmlResolver;
use DromParser\Utils\ResourceReader;
use PHPUnit\Framework\TestCase;

class HtmlResolverTest extends TestCase
{
    public function testGetByXpath(): void
    {
        $htmlResolver = HtmlResolver::load(
            (new ResourceReader())->getContent(__DIR__ . '/fixtures/index.html')
        );

        $DOMNodeList = $htmlResolver->getByXpath('//title');

        $this->assertInstanceOf(DOMNodeList::class, $DOMNodeList);
        $this->assertCount(1, $DOMNodeList);

    }
}
