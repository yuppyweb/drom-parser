<?php declare(strict_types=1);

namespace DromParser\Test\WebParser\Filter;

use DromParser\WebParser\Filter\Exceptions\InvalidFilterParamsException;
use DromParser\WebParser\Filter\FilterAuto;
use DromParser\WebParser\Filter\Types\Auto;
use DromParser\WebParser\Filter\Types\Brand;
use DromParser\WebParser\Filter\Types\City;
use DromParser\WebParser\Filter\Types\Damage;
use DromParser\WebParser\Filter\Types\Documents;
use DromParser\WebParser\Filter\Types\Model;
use DromParser\WebParser\Filter\Types\Region;
use PHPUnit\Framework\TestCase;

class FilterAutoTest extends TestCase
{
    /**
     * @dataProvider regionFilterDataProvider
     */
    public function testRegionFilter(array $regions, $expectedResult): void
    {
        $actualResult = (new FilterAuto())
            ->setRegions(...$regions)
            ->getFilter();

        $this->assertEquals($expectedResult, $actualResult);
    }

    /**
     * @dataProvider cityFilterDataProvider
     */
    public function testCityFilter(array $cities, $expectedResult): void
    {
        $actualResult = (new FilterAuto())
            ->setCities(...$cities)
            ->getFilter();

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testDocumentsFilter(): void
    {
        $actualResult = (new FilterAuto())
            ->setDocuments(Documents::OK)
            ->getFilter();

        $this->assertEquals(['pts' => 2], $actualResult);
    }

    public function testDamageFilter(): void
    {
        $actualResult = (new FilterAuto())
            ->setDamage(Damage::UNDAMAGED)
            ->getFilter();

        $this->assertEquals(['damaged' => 2], $actualResult);
    }

    public function testUnsoldFilter(): void
    {
        $actualResult = (new FilterAuto())
            ->setUnsold()
            ->getFilter();

        $this->assertEquals(['unsold' => 1], $actualResult);
    }

    /**
     * @dataProvider multiselectFilterDataProvider
     */
    public function testMultiselectFilter(array $autos, $expectedResult): void
    {
        $actualResult = (new FilterAuto())
            ->setAutos(...$autos)
            ->getFilter();

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testAllFilter(): void
    {
        $actualResult = (new FilterAuto())
            ->setAutos(new Auto(Brand::SUBARU, Model::LEGACY, 3, 0))
            ->setRegions(Region::OMSKAIA_OBL)
            ->setCities(City::NAKHODKA)
            ->setDocuments(Documents::OK)
            ->setDamage(Damage::UNDAMAGED)
            ->setUnsold()
            ->getFilter();

        $this->assertEquals([
            'rid' => [55],
            'cid' => [98],
            'damaged' => 2,
            'unsold' => 1,
            'pts' => 2,
            'multiselect' => ['7_119_3_0']
        ], $actualResult);
    }

    /**
     * @dataProvider filterExceptionDataProvider
     */
    public function testFilterException(array $autos): void
    {
        $this->expectException(InvalidFilterParamsException::class);

        (new FilterAuto())->setAutos(...$autos)->getFilter();
    }

    public static function regionFilterDataProvider(): array
    {
        return [
            [[], []],
            [[Region::OMSKAIA_OBL], ['rid' => [55]]],
            [[Region::OMSKAIA_OBL, Region::PRIMORSKII_KRAI], ['rid' => [55, 25]]],
        ];
    }

    public static function cityFilterDataProvider(): array
    {
        return [
            [[], []],
            [[City::VLADIVOSTOK], ['cid' => [23]]],
            [[City::VLADIVOSTOK, City::NAKHODKA], ['cid' => [23, 98]]],
        ];
    }

    public static function multiselectFilterDataProvider(): array
    {
        return [
            [[], []],
            [[new Auto(Brand::SUBARU)], ['multiselect' => [7]]],
            [[new Auto(Brand::SUBARU), new Auto(Brand::TOYOTA)], ['multiselect' => [7, 9]]],
            [
                [
                    new Auto(Brand::SUBARU, Model::LEGACY),
                    new Auto(Brand::TOYOTA, Model::CROWN),
                    new Auto(Brand::TOYOTA, Model::MARK2),
                ],
                ['multiselect' => ['7_119', '9_4', '9_7']],
            ],
            [
                [
                    new Auto(Brand::SUBARU, Model::LEGACY, 1),
                    new Auto(Brand::SUBARU, Model::LEGACY, 2),
                    new Auto(Brand::TOYOTA, Model::CROWN, 8, 2),
                    new Auto(Brand::TOYOTA, Model::MARK2, 4, 0),
                    new Auto(Brand::TOYOTA, Model::MARK2, 4, 1),
                ],
                ['multiselect' => ['7_119_1', '7_119_2', '9_4_8_2', '9_7_4_0', '9_7_4_1']],
            ]
        ];
    }

    public static function filterExceptionDataProvider(): array
    {
        return [
            [[new Auto(Brand::SUBARU, Model::CROWN)]],
            [[new Auto(Brand::SUBARU, Model::LEGACY, 15)]],
            [[new Auto(Brand::SUBARU, Model::LEGACY, 3, 2)]],
        ];
    }
}
