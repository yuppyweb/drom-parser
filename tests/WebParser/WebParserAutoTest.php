<?php declare(strict_types=1);

namespace DromParser\Test\WebParser;

use DromParser\Result\ResultAuto;
use DromParser\Test\Helpers\MockResourceReader;
use DromParser\Test\Helpers\ReturnKeeper;
use DromParser\Utils\ResourceReader;
use DromParser\WebParser\Filter\FilterAuto;
use DromParser\WebParser\WebParserAuto;
use PHPUnit\Framework\TestCase;

class WebParserAutoTest extends TestCase
{
    public function testWebParserAuto(): void
    {
        $webParserAuto = new WebParserAuto(
            new FilterAuto(),
            $this->getResourceReaderMock()
        );

        $webParserAuto->run();

        $this->assertEquals($this->getExpectedResult(), $webParserAuto->getResult());
    }

    private function getResourceReaderMock(): ResourceReader
    {
        $returnKeeper = (new ReturnKeeper())
            ->addReturn(__DIR__ . '/fixtures/auto_list1.html')
            ->addReturn(__DIR__ . '/fixtures/auto_list2.html')
            ->addReturn(__DIR__ . '/fixtures/auto1.html')
            ->addReturn(__DIR__ . '/fixtures/auto2.html')
            ->addReturn(__DIR__ . '/fixtures/auto3.html')
            ->addReturn(__DIR__ . '/fixtures/auto4.html')
            ->addReturn(__DIR__ . '/fixtures/auto5.html')
            ->addReturn(__DIR__ . '/fixtures/auto6.html')
            ->addReturn(__DIR__ . '/fixtures/auto7.html')
            ->addReturn(__DIR__ . '/fixtures/auto8.html')
            ->addReturn(__DIR__ . '/fixtures/auto9.html')
            ->addReturn(__DIR__ . '/fixtures/auto10.html')
            ->addReturn(__DIR__ . '/fixtures/auto11.html')
            ->addReturn(__DIR__ . '/fixtures/auto12.html')
            ->addReturn(__DIR__ . '/fixtures/auto13.html')
            ->addReturn(__DIR__ . '/fixtures/auto14.html')
            ->addReturn(__DIR__ . '/fixtures/auto15.html')
            ->addReturn(__DIR__ . '/fixtures/auto16.html')
            ->addReturn(__DIR__ . '/fixtures/auto17.html')
            ->addReturn(__DIR__ . '/fixtures/auto18.html')
            ->addReturn(__DIR__ . '/fixtures/auto19.html')
            ->addReturn(__DIR__ . '/fixtures/auto20.html')
            ->addReturn(__DIR__ . '/fixtures/auto21.html')
            ->addReturn(__DIR__ . '/fixtures/auto22.html')
            ->addReturn(__DIR__ . '/fixtures/auto23.html')
            ->addReturn(__DIR__ . '/fixtures/auto24.html')
            ->addReturn(__DIR__ . '/fixtures/auto25.html')
            ->addReturn(__DIR__ . '/fixtures/auto26.html')
            ->addReturn(__DIR__ . '/fixtures/auto27.html');

        return new MockResourceReader($returnKeeper);
    }

    private function getExpectedResult(): array
    {
        return [
            (new ResultAuto(52088341, 'https://novosibirsk.drom.ru/subaru/legacy/52088341.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(300000)
                ->setGeneration('3 поколение')
                ->setMileage('423000 км')
                ->setColor('фиолетовый')
                ->setFrameType('Универсал')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/cZXi7exlB-aqZULMXnBMu0KaW8-GRZR5TuCo0xONGZhgeWg55dodoyrDmehWEwuRZmAyO3syOh2N5C6juA7qIXfriQOF.jpg',
                    'https://s1.auto.drom.ru/photo/2ER9ua4Vb7Cw9aFtUTpmCpLKQsV5bTe4xOnEKBtxvkazBo-wSoHE_lxID28KsK8pQ-BrDjLytzHWkx0MKD0Ris9g7Lwl.jpg',
                    'https://s1.auto.drom.ru/photo/UOuj-cGfkIIOhPqs4Q1nL2oFcx7oL4B5R8hqkNpsOUsuPB1YUMfJhL9veAprhygjvuRj9e4TTJgEODT-7HgtRdmzYg12.jpg',
                    'https://s1.auto.drom.ru/photo/wIdFu36ls_q1nt9CPMykIsGqvIfHyxOdwVksnA6_L1sozAv2M-ZT1tnE7oNYC2I8yAKqEliq1HQw-_6ygVHeqqLy34HC.jpg',
                    'https://s1.auto.drom.ru/photo/ZDwtPbIRBmyYx6s9m5xvuM6c4SBUtJ6B-HGQisFWMGals4K4LEQZZFUs_uyb4yjhwTO5tslBlt2b1TYTjxQdttYh7LuA.jpg',
                    'https://s1.auto.drom.ru/photo/19F-QzNZ47P3rDlSgsOjwEjmV9OgnwHiNT0Bk21g36PkMT78RBpf1e4BOO76AeIafRG0PAb-iaQbiYKzuvoFyJUuI6zP.jpg',
                    'https://s1.auto.drom.ru/photo/cVlvzZnVL5DgSqIV7FVe5yQ_jTeMax4qTfsIkf2HB47FKR7ePhHVzbnSHfg1XMPSBph9789wiSUS1qRy04DlwKk_82Oq.jpg',
                    'https://s1.auto.drom.ru/photo/Cv-PTHQFi73P9UC2RJ0TNluZZUTo-h5Lw8kIzasv7GssODAJHgenneLu1PUueMoyPCclElYPf4mq1opjetKERjAHjZfI.jpg',
                    'https://s1.auto.drom.ru/photo/2L9bGvRjZIhfld2QGotPomZqaZ8MAzdKrCt51m6h1fsb5p0tAd4HUPMPt3auHZMjqQ4ZyWE2Kb49omlnJrhDQait8l1N.jpg',
                    'https://s1.auto.drom.ru/photo/ZfSda3-DJmLYUiCLCgpZL0iqyenzukpvY3AZOumkEddMLNrtoMHKT4qXZM7jT774KLeT-3M4i6htpcg3Xy9qG9Xi4Pe7.jpg',
                    'https://s1.auto.drom.ru/photo/ccDtHhw3fOQA3aP8Aws7S3or-248rkilFwt9c9cYbg-XKfhRYKQC225LiSep1B1F5rgQCFV9SUnGlyqHXXiNaIhshFlX.jpg',
                    'https://s1.auto.drom.ru/photo/4kGnuWmwAm-qvMuZKE4T40146BYcXhHmXML0Vx9c8Zsv5btMfgCKncDm9hI_m3NElNj2cv1cJ52qdGuVW7KXPGLGLqpt.jpg',
                    'https://s1.auto.drom.ru/photo/cC39LlhewDnpky7LznWGvRn9ZM-dz0ujUc5IRelpCq43NwiaIEJSBbPQO823yuf4Mz0xY66Ynay4cF212v7sLzURe43M.jpg',
                    'https://s1.auto.drom.ru/photo/0bFstByVWlIybPrhx43LGt_1LUbRRWtHlg-9EQq_2VNYKtSU9zZ3JzTtoGIt1aUMAPz691tOaqMUKXY3ifpB7sdBxW7J.jpg',
                ]),
            (new ResultAuto(52077708, 'https://irkutsk.drom.ru/subaru/legacy_b4/52077708.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy B4')
                ->setPrice(210000)
                ->setDromPrice(395000)
                ->setDealType('без оценки')
                ->setGeneration('3 поколение')
                ->setMileage('400000 км')
                ->setColor('зеленый')
                ->setPower('150 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/-JVJgG2pOklYwCsQKTp1UMToMruTa8k9YIo9Ja2pFamrizPr4kQEWeV1237ijVzUPCV3odw_dEsLfzStQCLlGOoRmna7.jpg',
                    'https://s1.auto.drom.ru/photo/tWYOAhDR4QznnjnHsN5TaYoYN59igEeqeqrLX9MW0-1V_SSL7ST6PEMxgB8O2D1K562Bkjvcsz9yTAEetTV8cSA8jM09.jpg',
                    'https://s1.auto.drom.ru/photo/7lDfDPdejnepeJDPjHB8mb5f4AD9PiSjDPiugt-beTdzNm3Ya9IJJMNGVdARUk_p0Mrx3sIpl9HtVbJSenC-yvxNpIpc.jpg',
                    'https://s1.auto.drom.ru/photo/McbesQ1-gggHP20vbm0z_u1L9tstUlVJGl1VVXScAfHsg-SpzIqbDKoNANw1Snvkv6yZJv71VXUGm_SeR63NULV_edlO.jpg',
                    'https://s1.auto.drom.ru/photo/IJ-qcPMv3MAOUPaaqn27laxsUkI6sWciuwTOnqGLMmes3sAxv_XuAOZpN5KY_TR-AzRzdITSaxCpobwT2JmASWR_ZiKA.jpg',
                    'https://s1.auto.drom.ru/photo/7wEoDVGuU9O0pdjTfIYO6OHWF988oynxQoDD4Lt5oXC7ncNiFBs5aEtIC2PUnEx_iOi3KIIXPo0Z5OzoL6nBnOG_qBzD.jpg',
                    'https://s1.auto.drom.ru/photo/_jjjmPO6xXg9JqDQ6D6oRNxCsaL8r-lwzFJUNKHGIMmArZgpe3uffKg0zKgGxsqNZ2GxkHoUzQQ5yhCRI3kaz6Y8s9Bu.jpg',
                    'https://s1.auto.drom.ru/photo/zo0TOBOmOok9aMyhg0JnAbxstr-ZHEO_-2Drq1f1fdFiiqTC7vrxUIhEoH9q14gTMR7uRNK87F0R72rxMIJeq3PLvoz6.jpg',
                    'https://s1.auto.drom.ru/photo/2gMfoWWd3IVXb3mzm2QWvgSQDNIEKGlbqC1AKteen0ZxFoKeeKiKdAE-t89g4cJi6Ow1nw31DONmPNnI4DrygO9vq3Ks.jpg',
                    'https://s1.auto.drom.ru/photo/hxB3bX4zY9eBvMoRPDoTssrp98MIXoEo-dAG9YPZhnuipLn26bROOKSU2eIuHoXp13--Ic5eBrm22Jv7O7mUtt1LUzxZ.jpg',
                    'https://s1.auto.drom.ru/photo/m9yNvMoo3qK-S--NvO90VhneCyOrHQSxTIoTcTA7iGFTAc-0cS5SniUJlOxGydeWXwF1BfFORjiuotxCgWS_bsYBaOvC.jpg',
                    'https://s1.auto.drom.ru/photo/oskP77Fhi03gS8SKkPFDuq9pjnlnsLHv6C12L5eFmd4_go3f37hf8-Wt5Owz9NMqjGdvsLAuqJmwIyqY9zpcEXanHZly.jpg',
                    'https://s1.auto.drom.ru/photo/LcyFMK8SBPXtP2zCGgS4njLEQjNpQeHu5ke-JZ7OW2digCo17sPr5Yr0q6mkDwCot71-Zdi9eaKlrIoAiS0ADNfAEym-.jpg',
                ]),
            (new ResultAuto(52071251, 'https://kurgan.drom.ru/subaru/legacy/52071251.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(575000)
                ->setDromPrice(365000)
                ->setDealType('высокая цена')
                ->setGeneration('3 поколение')
                ->setMileage('260000 км')
                ->setColor('зеленый')
                ->setFrameType('Универсал')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/Ir0gkKWCGEoxVU4HL3dtiOZWL9BgssLz-GAJ4QLRKWpgq8xrvf-oVqU_fnZF4eBWHdLMP3royH39ncQKC87LZ3s958M_.jpg',
                    'https://s1.auto.drom.ru/photo/gFfgT9mt3Yj3-KAiyTKDf5Pr5v-2ZublGLMmg7au8ZwhDVk8GulZ2k7WbEU7JE6SBkedjqgn9uINcO5tAPJn0DX2lHKx.jpg',
                    'https://s1.auto.drom.ru/photo/nj_BJ9cI9rbcE8nK_GbCsNB7aKF8HU6_rn66F1298ULwk0y6DhCmA0z46uakHqKRdfn-GmKFlSDCTSW7Zdk_A34Q0AM2.jpg',
                    'https://s1.auto.drom.ru/photo/AxKPmxFE0y6MsVqJDx4ni_-aV_p8gbBFLS4n72w3eIt43ZHwVX-TtxK8hZau3n3eQSLViUFqPoxibtF3H1jm7oLwM4oV.jpg',
                    'https://s1.auto.drom.ru/photo/CWJMnxhges6-3J_y3MqTtG7Pv2e_Bria7Z1UPNMdc-hJ2yu2pWTb9EutRxbAzyic5RTarvoug9IIzd4nKB4ZhNFB7_gu.jpg',
                    'https://s1.auto.drom.ru/photo/jfhkAbDgALsWYobwvdgspIob5LRNZcvMO8S_HfPfdGxKkTUfnSkaLvDTcf4wHPUuXT1eSZpTxtLf08kPyBLvrpzvpXaR.jpg',
                    'https://s1.auto.drom.ru/photo/9eRBP7OOicmXVsSuIU_06E0Kaho5-GzrCNasR7hTS2qcKvSnBneTtNloqRHbJZIEwuI3DZAiauHP7HHTlItG-xrBRhuP.jpg',
                    'https://s1.auto.drom.ru/photo/W-WPW8ojFOhf-xBZUPyyHw-RAS6CxL3-SVmCgTRnAWcL0Rk0H30uk2GDDRmJARcypu95sT95SRsS3BgWLRmyamXB3M3p.jpg',
                    'https://s1.auto.drom.ru/photo/hIEmknOUsi1EIqjs27fBvo0d3RoMpbM3tAfwrDkkjFXiN60bk8ihPdWLAo2izn-lKPCuJEVJr6gRmU0qjSbGbxKecnVI.jpg',
                    'https://s1.auto.drom.ru/photo/GX2VuCFFLac3OpFUbHPMyPzQ4fTzpu6AYt-GHq_Wvp0dRyuCOObmS8pBesI0_SLvVCFEgeG3olH5pAhUfBhdIAOU-ZcK.jpg',
                    'https://s1.auto.drom.ru/photo/je_ONi2GbD6p94ibtdgXwtJ7NPxW27ncGajhxm9PwgB_zTDly8mIIenaiawlXA7a-zpzjP0IOdxrL-eQu0gtES9DFpde.jpg',
                    'https://s1.auto.drom.ru/photo/IPD7PcgyZW7YlcmiBZJqr8NKGFCYlHnKJrV4W_Pm2qPc_D2tlZvqiXA65wASsb2h4RlFssuNT_1jVnsg070MKx2q7FTd.jpg',
                    'https://s1.auto.drom.ru/photo/t7fVYKIJqhFdvm2nh_gVXESOU4JLTljb1S8LduZOk8C8vzrGMUMmz6RwxLs-Y-15nowMnc-LanzncK2z1htUoteKnZ1J.jpg',
                    'https://s1.auto.drom.ru/photo/5yc8DRDUGvNd2AMd90qodKQsFuNkNmaemTFKgbSi0R8hbKa6BG3wI8PUTc28h_sI4uMgE0dlvIxoZHuT3te3tGfs69a_.jpg',
                    'https://s1.auto.drom.ru/photo/T2iuVBIxLEsGW0oYoV917Qy3eEDIjIHqeTiJzCFIM_35gAeukM_tGTifSVemDvBVSwlCNGsHvjGx_58m7R2Z5ITS61ze.jpg',
                    'https://s1.auto.drom.ru/photo/sLCCYc4GFA9Rg2XpKPIV-yy9C7FP_G1J77xHvPMN2wOHC2YvCLBCCokJK_IdB3MHfl31Ym-oFgIsU-2gBebCUhqfTzXr.jpg',
                    'https://s1.auto.drom.ru/photo/OVc4O5bCqIpvogBeBLFO3ErE-IJ4bHhrH82DIWzyt80Kn4Yk3-T9b84Dx4ofrTr7JNPEYLlLw6WWyRnUztwOjEYXuYir.jpg',
                    'https://s1.auto.drom.ru/photo/Y02braxHHY8Y8Ef6zP1JeqA3m8RZYB7bAv6sb2mv1so99vrS02oBA4pZtz-5AGU0qrzOnUurilKpT6XYmErXNYEXvtGI.jpg',
                    'https://s1.auto.drom.ru/photo/so1HWtU4QRbbyMTv0FAqiiKNZkfE5g0-PzIaROfF-BagxqsAXx6uNNWykpn0gxB9SYpH4mqaaLYdSN24GTpQmvslWTR9.jpg',
                    'https://s1.auto.drom.ru/photo/TdmslCbI7l-UfZHRharmdTC-DgT_9zW-wd5E8xW1L690rMKFJG_odxCYvAzv06RALPwk6kuEQ_xJanXipbrAGQBUt8FB.jpg',
                    'https://s1.auto.drom.ru/photo/B9r38oId2Rd6jHwrlbWbxTYUmjzMZvtOVFi-AZBBaujm5M22f50d5W-as-_53thD_aCwlRST7BmgaUly1PzRo8LVAWfU.jpg',
                    'https://s1.auto.drom.ru/photo/Piw9ec6pxXnFbY0vgo_kz4jIOhcUyDekmHvbRnqys-tLL-gsdpTRW_0WIfSH3YW0-ICmR-yhEe6NjjGryDJqXHH57qXU.jpg',
                    'https://s1.auto.drom.ru/photo/dAtqoMvJ2YPx9Em_w9faW1ykpzxYfwaa6QKcSG1krflQ-zwALPBLVSbXMRke6fEy3OyEQbl1JiRMAIqhgXYsTDPXs5rC.jpg',
                    'https://s1.auto.drom.ru/photo/IULzQGKpiYfru1oCOayu3ezDRIu83Cab0PPIm_M9vbcuC0acMNZs9UC1gmnVWC95Yt5us1sB9QM8SgfODMD0RrAxSKs2.jpg',
                    'https://s1.auto.drom.ru/photo/NJxvBH-zCMjA0BtXmGq1SPCugPvO8Iflhy9kS1gnp8J-c2meZ10PimqiBu5vLWBIIrWCxTNzPr5oa2sOcq12v0ChDOlq.jpg',
                    'https://s1.auto.drom.ru/photo/4RLZsnNtFhWQeur1z-rusltGozUABBjbEer7mV1k0o7VEfb2K37_MD0wVOs0z-kXVJllJB5bMb1pzyIJwR4i6c6yuzQd.jpg',
                    'https://s1.auto.drom.ru/photo/uuApIhU-_o2nMYjQNh-GU60fmU5mwq1JkB7vlcvVTiPgAFHRK711vrlcGyV8ws1zyRgdju9Q0xUxmqt6Jvm55ifgl-I0.jpg',
                    'https://s1.auto.drom.ru/photo/HqvCYh2is83JilBPBEGQBKILu0M1KXQ8P_2XqeZ-1yNQ-i3_UpdxncKjj3W5AF8IFQt5XBjuNwcuEU6tLXZJjnh3c33X.jpg',
                    'https://s1.auto.drom.ru/photo/RkDV1T7v4KtL1t3fgputaYjEx7HZ5T_3GuoMigTPO8iPpcB9WTZM4FUcfb1samf0IYA_00cvZTTskteP-BbRpZbvMy6p.jpg',
                    'https://s1.auto.drom.ru/photo/iU53ah2hFu8KTatIpzq4lStSp1j4X0jPLy5WgU5prSbwxB4KPBv_EQpJSlZ-KkKEU9NKnos8rrVk-ampdpEuFKfDOX_V.jpg',
                    'https://s1.auto.drom.ru/photo/1aDVVek_u0FBZ0XVqM0_RViyhOuUOWSAZdL_LLq_0GqY8Sdmigpu5BZ7gU3BSANGE6wa6XNO27s4ec2ZxB3ypZxqV5A-.jpg',
                    'https://s1.auto.drom.ru/photo/SdUNF54bCgpgUMOXi5gl3KAxaHx0JyqiN918BKwp7DLqOC1iuRA5qFnuOj_5TC84a9-L9fvn6ZDkWwSQvpb8SgeZw9dF.jpg',
                    'https://s1.auto.drom.ru/photo/fQWcwJo5RmkQgb711etc_qp9nvxbCJqPlXjZjwE4jepXX4zonIDHiP9RbHfzIz3wRi4-Yqfak_HlAdz4nXfbTFrOusSC.jpg',
                    'https://s1.auto.drom.ru/photo/ariap2FJicGpFYf1uZlt41R-Rpj5f2wstXUaaM9wGg_qrFDH-5l6qc69vqHmV6uc4Ym1gEXH-60wnCjxI5XYEHf3XIZj.jpg',
                    'https://s1.auto.drom.ru/photo/YqP71FnJ2n2YMYqL-jkeqqmujL0YpO6JTaQ3hpsHXgwiWAChRmdIepgKVKxDyFLeqdpdQ1kgFw2VbvQw8CxtAgyRYjL4.jpg',
                    'https://s1.auto.drom.ru/photo/vbCW4cjyuLvQ-ssmNdXta9t3w8yXZVRJQqfSMXkbKXdjiMxeEYOwErWZexERDyFsMAWxPi68KKsXKVtAOyvmmlnU0KKf.jpg',
                    'https://s1.auto.drom.ru/photo/T2Y0cA9Yk3hxKsRu6PiN4p0nxvb8vXTY7Y-cKJqyyj568xc4WJSt_tVZXUQ87L_Dow0ofPRuLgeJOVSu4kZdVGPd3a8F.jpg',
                    'https://s1.auto.drom.ru/photo/0m_AuLYIE3YhPYjluPimu9bzplvk_mVkxyphzMO1IIQ49vsHXqYSiXBctE2l3myFVSE7MIOHhY8cpiaxtjfDxJoCprsV.jpg',
                    'https://s1.auto.drom.ru/photo/zTB4OjpkYRS1YD-A5-0b5A9EZORmpBmCoTZZGzYGmnmS0InYoeyOEAJuxQKXWKCHWY-LUCxxdtcdCgHNEaxf4WtFDoH6.jpg',
                ]),
            (new ResultAuto(51349260, 'https://rubtsovsk.drom.ru/subaru/legacy_b4/51349260.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy B4')
                ->setPrice(400000)
                ->setDromPrice(380000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setMileage('300000 км')
                ->setColor('зеленый')
                ->setPower('115 л.с.')
                ->setFuel('бензин')
                ->setVolume('2 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/_Hq96KaTwd02IlEnsAZmjSLIbtxW1MGn44n-6uyhquKMMhmFodMJsHMoTkxSIlrZu0N0T8hzHnTzBBJ2v1MxJM1a5UiL.jpg',
                    'https://s1.auto.drom.ru/photo/TJnV9eKlkpxpyVMdoN_0g4stqie-Fz0qXON-VE1a7XZsyQUV8TlDytO38xJWufd6s9J8mVW5WFDqJvMWeNUS3z8c2-Vk.jpg',
                    'https://s1.auto.drom.ru/photo/F6iNbrixudSTmG-m7i2R0Fg4lB0kfD0Izlqu5W_qHzhZbo8r22Tw_rLjJZJnbAAmyfFHC155qOvhlIb2VxeBvt0wE9_g.jpg',
                    'https://s1.auto.drom.ru/photo/1EcvIOOC82OIbGg3RCADaBFD0ORTHrsYUuSeu27fwY1x7WlU1KQfR0-0X3XaLBQHfWNv8P78iV9QIheEvf6SkL_x3_Fa.jpg',
                    'https://s1.auto.drom.ru/photo/VplJIUyhIvz604DLN2ExH9TBZoSC--A4QeB2OLHKriTX5N42KT8X5ffwVnTnUHkdLxvZYymBxcfzBvB09R__Z-1KtR8w.jpg',
                    'https://s1.auto.drom.ru/photo/tefebBrL9wasLTgg0PNeqBK81Vhr61eG0WKklz2Vk1ARCsXzwJjMmTYpJVzM5ImOpFQ1IMU_sg7p7eLV2rFpZqxzMOk_.jpg',
                    'https://s1.auto.drom.ru/photo/6Mbwewc1QLWH-vC6p3KaJbQDA0EQ2Cuyx2pEt7AuS4nLD-c5sgBnO21_d0og7uAUilZxKAVrmWLpS2pFl-230Lu-PtzN.jpg',
                    'https://s1.auto.drom.ru/photo/NJfBPhGMZ8p5wwrsvxeQiEGbjE1HiOASQVF1MnogmMA2jiwqcs43C2WypGiXYzt94Am8C6XjqNf6JvZzj65L5CgwxvHj.jpg',
                    'https://s1.auto.drom.ru/photo/-JSl1bEACHq8pK-hJbOHK9uFm7EzMIgihvhdfSNEPPJA54joKBbhJgT7CT7J-5HAxse85XFRiN_OHO5rqO2wxrt6FeMJ.jpg',
                    'https://s1.auto.drom.ru/photo/VxJhoqdsGj_9zB5uCR_nFoYkpKfcZERDJsbPuHUoawZjEqzDGc3Rh2p5seInay_Zfid8IzAny5JOjluge62Rcbkdu4LN.jpg',
                    'https://s1.auto.drom.ru/photo/bdLTA2sHJjzyyd_cwhTL7S1M8H7NR9-m2ysqsnZiRdGD7seITjIcnoVOpvZjfrstQz8MKVDBrvMfCepnY69pyvvMpgYN.jpg',
                    'https://s1.auto.drom.ru/photo/uQWZtR1QldRECxFuwJw4COnY_RtbSA1KeMdXiAB4YmGyJDIpNlKru79JiHjqGkfix6-sBesJ7Qmzrf3faDdnbyv88yZj.jpg',
                    'https://s1.auto.drom.ru/photo/hjN8h6uqoBnpdtgEcKtvXHjNn4449GTjjLldOT9vNljV_jHr6usMkRzyTVDJsWgrIykVi8v8LXeZpT8QTk150ftHgCaC.jpg',
                    'https://s1.auto.drom.ru/photo/d3o7VbTFK9wFNZpHyoqaJt2c3Z7m48kRboRWiulWrNAX243ipWvRmfyChFrqRvazLoZGppJ-YudFb6Oj-uOmD3Sgq-x4.jpg',
                    'https://s1.auto.drom.ru/photo/rjNPId0Gp8JXsONuXcVgGaIEP-OsDhv9maRby8RAng-lauVAG_vaQdhnZMeCQHS4ugeyoxWsstp8D-BmtkLzw3BIF7CF.jpg',
                    'https://s1.auto.drom.ru/photo/dggQdA13wHyCSNT3xOgEBz45wnRJJ9Ork871LG-vmDtG2cX5Fq2KwDgdZ1k3NeTe5S6Md2BjYGz-LZwhvRLPbyhrjvxL.jpg',
                    'https://s1.auto.drom.ru/photo/98HHrnVw57-vJGlMFwYqp5eOCFqf0m65ds4_v2wlf8FgbWMTCWHeWHbmFDMKfCjw5cJ89NoApgrevrglRyuxyB7sYNAJ.jpg',
                    'https://s1.auto.drom.ru/photo/Kvd2icbCfA-BzqcTf9dYI2SxOQCrV7fb3-u-fZ3JmPE1oNr-mA1GG5wkMWW9rEn8gvj9RojsF9omz7tClJmB7WOtPY5z.jpg',
                    'https://s1.auto.drom.ru/photo/_MkNliofMZMGBh9I04zQzkrkTQ1R9-qXCtjYbP9Y400CetYrZ03Beh4iZ84GEjcAzEMWwd2y7-NPMiUEfKX3FuHZzYfG.jpg',
                    'https://s1.auto.drom.ru/photo/6C94FFCYxcCaetz-Y6i3CfsHsHPBL59IjF6eBj7iOI68PIzQqke5xIXeAGE-uUMthMlIzrfjvy6RDZB1l-et_09zp3Ko.jpg',
                ]),
            (new ResultAuto(52061318, 'https://polevskoy.drom.ru/subaru/legacy/52061318.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(420000)
                ->setDromPrice(420000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.0 MT GL')
                ->setMileage('250000 км')
                ->setColor('зеленый')
                ->setFrameType('Седан')
                ->setPower('125 л.с.')
                ->setFuel('бензин')
                ->setVolume('2 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/j-T87bGaimgGrNIUCCgPTCPJnxqbzf0dCxgc--QRnC3rkLSMUNyPbPTMkIjbhC_eI_voW7xbut8rTcJekgB7VTYZsGFk.jpg',
                    'https://s1.auto.drom.ru/photo/INWr3qc0dhbeKWPhJi09k4rXiAKbxd26DdtVF31Xx9faf6n4si6nLyk_pPAXeFjmN4DSUWpS8kAMTGCaZ5bpGuTBUPhF.jpg',
                    'https://s1.auto.drom.ru/photo/cTS0H0wO3ktcjEul3TT-a5RI0R4--ONyKTW-JqGlkL2OIRDP5Q5Utwl61dlzT3cPrOL6ms4d6BYmyeZ8kZW1pr6n4IH7.jpg',
                    'https://s1.auto.drom.ru/photo/UqzP8HlDeTbBQnvYLJdfj_cJZMTuLRY1u-anx3X-2JQ6MdyDxffBFdX_UTXIwXKfUakYdz5ZEP_EBX9AE7SZL0ACo5my.jpg',
                    'https://s1.auto.drom.ru/photo/W1rzgcLVL0tpR1IjJo_J7qZwmOMYqpmvS7EG8brzxmgUaOOETPm3_pZX1Ggjaxceh8AFeMd7Yg58FMN2pXuqLmqNvUO6.jpg',
                    'https://s1.auto.drom.ru/photo/iaDxG2AtjJ-M1GKQUmzEhMCoGQSe7RUshMTlupdFQ9gwi7baFE-STeh87MHrZ0gzF3CNXK24UEfLQ_tjCFjsLenWDUfl.jpg',
                    'https://s1.auto.drom.ru/photo/i1jzFIdbetbD-iZdJOc-_bHMMyRgmxHaUmMpqvlByAsF241huR7Z4VRDFeKlnmDYY-hKTAYO0b6kiQkv1YXLRbcISo-f.jpg',
                    'https://s1.auto.drom.ru/photo/6XYXv1X_tYkaqmLr9xUIXL9jUC3tD8xI6dq5nZV98Mkq0qD14xFtlGbYTrpNbvagqfbxivdpgLP72ksTQHPPFozCj6Jb.jpg',
                    'https://s1.auto.drom.ru/photo/onfYe5rS3OvlBlL1Lq7-DYjH76nJutT4Kq1_wW24SGaP9gUadUBijX7NUJiWSFEJdI6wEbyt5nwuTEVw1KJiZy62cMBR.jpg',
                    'https://s1.auto.drom.ru/photo/W4Y9Ht-h6C3Jm3X7IWeG5e6MbblMvAaef64dPzST-IYyeTey298ETbusz6J9y8pUDApQ1XcP4XMv-Z1J5LbuQ1KlU-mZ.jpg',
                ]),
            (new ResultAuto(52049375, 'https://omsk.drom.ru/subaru/legacy_b4/52049375.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy B4')
                ->setPrice(355000)
                ->setDromPrice(385000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setMileage('350000 км')
                ->setColor('серый')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/UWtbjO1EUtRSztq3CvkHa8lh6Wl7ZMeCQmP1Lv4uhH0uNTJKLJ9j3AZtQHzU4iZQrUJZVEA4oVRPBYJALeCOtUm7Z_A9.jpg',
                    'https://s1.auto.drom.ru/photo/p6Psi6hjicT3gWVodR1UJ6pj48o82jU3iHpPfx804MHRUloAtJgd1wFylu-OnkAnnQOpP02zfMBMP0NUIG7SIjWG4ydZ.jpg',
                    'https://s1.auto.drom.ru/photo/iAmFA7XAoBnj8jxosIfZs9WCgb96RcAFqVAwFWNZmFAqVHh4gNylyPEzuzPVBN78mKBJYdu65RE9gOC8Xv4uOkVwyQJP.jpg',
                    'https://s1.auto.drom.ru/photo/qsXAUGrGbxOGsa3J1t8_s5LTytrCL4BKYU8ms83NJe9f7b50lGEpIjIEqP_m-q8qRlAVcqWADOyuIqYhbNUaUXxRAYmH.jpg',
                    'https://s1.auto.drom.ru/photo/KZaHRRsa-jw-r9hWoHGb0nBJYU4-BdG5a_NafvYk9jbzmfZFPSXJZH9QJEvmVKjul525tmwprh6mOS8pPlxz-Bt_0HW_.jpg',
                    'https://s1.auto.drom.ru/photo/RmvBUtOSNbW4FBxLB6G02JZOYS0eoL_Pmh-RLHxKSn02VPByaFzA-H6sr1e9ba24Y9Yeq2md9kq6WnPmtHx26RBU65ZM.jpg',
                    'https://s1.auto.drom.ru/photo/sLYIPuzv_Vu0G6kRak20nuZKi1K7ZObpHK3WoyZLLyD7wBMeK3vNu-kUiHejMGsUeVj57q3ip5wTPGAAOoGHg4Av_qTi.jpg',
                    'https://s1.auto.drom.ru/photo/vZvLBra_XDQQ6GhfM8KMG55yVwI1si14qlLBLLUl0yg3unli77UsjbG1VkM0laRZ51jX5T7R1RS-3Jw0JuTVe4PLOAOj.jpg',
                ]),
            (new ResultAuto(51800866, 'https://krasnoyarsk.drom.ru/subaru/legacy/51800866.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(370000)
                ->setDromPrice(370000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 MT GX')
                ->setMileage('300000 км')
                ->setColor('зеленый')
                ->setFrameType('Универсал')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/AqXsVLXrkqqNEA2eA-oX_cEhdjXOJx-dnmMhZZsnouq2IWXjTVROwCs4Fpx88xYsnLQ8txDuYiARxlTKDk-rrmFm4eZW.jpg',
                    'https://s1.auto.drom.ru/photo/QpLCZe0-ARVcf7jckv_oXfyTYsCDwYMFLrzu2eYayGp7WCfwzLUHuY0lpFC7wQCzbRAjYHe6YHMWADFA2zaFLQXuHXS6.jpg',
                    'https://s1.auto.drom.ru/photo/oC4tpDaV7Mm_RZn_TguPFrjLW2BSA01xUQqgP_jkA5fD1pMFE8je6reXijnuc0oNJmuoyYgHbZZ57WmAlq-yOeuwv9ph.jpg',
                    'https://s1.auto.drom.ru/photo/FnGZZ1tg1V-6On-qfPgADm1HBddOkttY4M1pzjz9rbJdVCVIiNRQve1i8iqShM1fapHnRNlfN-1USEbjZ3_5FNIa7Xce.jpg',
                    'https://s1.auto.drom.ru/photo/mX-z4SuUmsqFhwRL5nKXZ670QD7EpqLRbvzQMKTnIEweyrU0Zag8T7zV_rPd-ZvyVx1iyncJw0NuQBiRSP3aCmzFz5Sw.jpg',
                    'https://s1.auto.drom.ru/photo/5OMuhA_muxF8ROZl_Ud4x_JW4g9Dd5ZdVpJh4JF-vbCYq-hBc_4eFvit2dthfeDamXOrXWBT3D2AVB2ig5V3QWZHBLKf.jpg',
                    'https://s1.auto.drom.ru/photo/o6bdY3dny9fiwyG7Lp8m38kIoo31TasYXJf99QTa74nc21Tx0h6-FNy8hlVxjaL8cKgIsETehitk3oE4EW-_VVNFEYTV.jpg',
                    'https://s1.auto.drom.ru/photo/AxDCph4f0vZzUSYaW18tC4wIXTH25y_MLoVbE5W9pN2urfj3fOGX593c3khvovfft6EWw0JoMRwSl6MrVwugdrBiO_IG.jpg',
                    'https://s1.auto.drom.ru/photo/CXjFC00-LyYOcijnOWe_pXqFQeBfS4OLdvwxV56cU7wEuHs4zw8wkw1vfEAKv6m9eGR6b9yZx60BAw7cLLwVIg_tCo-l.jpg',
                    'https://s1.auto.drom.ru/photo/Reh8nbmzzBP-Apyjg7LprqD2IqWDWtqRSfWV5lOecgJ2Hk5oAtQAUlo_6Ro8pWQavbXYogvqktyjMOQesE3Hfg5a7Nas.jpg',
                ]),
            (new ResultAuto(52041621, 'https://barnaul.drom.ru/subaru/legacy/52041621.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(470000)
                ->setDromPrice(365000)
                ->setDealType('высокая цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 AT GX')
                ->setMileage('250000 км')
                ->setColor('бордовый')
                ->setFrameType('Универсал')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/n_VHrXUNyFpajwTT8ZpkK07J7nMU7-Jmip5p9OwMzzj17ZF3YS39pKJ1wa1Ij7YpWuIOffkol9dx15jqJEi5HErMaypP.jpg',
                    'https://s1.auto.drom.ru/photo/kTAX3iBhkPVByIXvU4p_VAYrE2FaClksnRMCYXFL0H-vjbratssp6QZH5Q88m04TDyd2wQpDgn7kQR9EFtp3XaG_4osP.jpg',
                    'https://s1.auto.drom.ru/photo/iovSwenNZFKgG8vh0SaOi8y1U1vGz96ya9rxU9AjqKxWBvf5_XJAmVMrhoH0-K-NPxrXO6TJyfhjAJSQsOS2iHITefMa.jpg',
                    'https://s1.auto.drom.ru/photo/jQcMWbZiuhbBdhnoSZ6qm8A3e-DL9qXGSivlbhBTdEgYT1oXLksDq7OwtaxmdlXswHc6Ho3YA-rz7RfBKs2-RfYV7OM6.jpg',
                    'https://s1.auto.drom.ru/photo/ZORiSl0z-qc_IFshtUqnIINJOcpEtX--eTkrC60gczjYgREZsHOsz6jIxLhuMNLVz32vLGxjQb--qtk8vfSBVS2xQqCy.jpg',
                    'https://s1.auto.drom.ru/photo/zwlcpaU2jTK7baw5Iokuvu5AycXB98hW3KAxcuaiI98oSOw-yXeZT79XJ4z1uZ1T9mc-O0TxDIBFd77xHjUg9eiD30z_.jpg',
                    'https://s1.auto.drom.ru/photo/UaJLz2sMhUMinhx2rb_Sb-eZsJfq4ZtW01r8Mo9eXHOeEZfqED2Mcvu1BqvwO24BvpPkrbQpsXS_B9XnhdJvQG9GjAFp.jpg',
                    'https://s1.auto.drom.ru/photo/5ITh9SZjcjxBRgBu_w-NJMFNHklavmYYMCxh8EFT0XKC44GAB_mTtsb7JJGGxTOQh-6e5S3sTSDxLfUyRsgSoadMSxxo.jpg',
                    'https://s1.auto.drom.ru/photo/34BfcYq_5LsUcMQkJBowpYykWFyK4iBN32PBL8cuylVwDD_c9Zsr34kfyCxrxiqTaACvTVnFkc6BhPNGTHrUrXVccg_1.jpg',
                    'https://s1.auto.drom.ru/photo/uYrKk_lyKtkdFXn52IP5J_dFQpiJ21me65kU3BZ4mhuODUv00E6C0pqN3KjfDJrTgp4lT0DH8LriUFC_5f-RpNt8T8_J.jpg',
                    'https://s1.auto.drom.ru/photo/x-CFi6d9kBXNBlYGxxkLRgbr7sMYBwq1hjx2x16e_GyW2BUcVYYaXuSlTdjJW_AEjonDjrdHHC5B3oUAbeByWM_M7fx1.jpg',
                    'https://s1.auto.drom.ru/photo/WwKzDANDCkoUfxoLQB04BRMoExOkEeEscTO2awKbYIFgK-YVrRexEoiV0f9Kdlc1JE9u9DtbOXg5qzi0MTPywuGiK61c.jpg',
                    'https://s1.auto.drom.ru/photo/VP1KunNDifLxuGuam652feKJ0E2y1NVJ4-MwX2jR5U0SRHqkbt-OElFvAaxlBwIkka6axUd0iqSR7dvVmSsfuJ_teD2-.jpg',
                    'https://s1.auto.drom.ru/photo/y-kSMZ2GeQA4qSd2GZKx6VWqrr9mlYk0ZSfs2EvLv-FYyJgQbF7mk5odcz4VvuXYekjsbxl7EWLY8jnNthJW9X2-qOEw.jpg',
                ]),
            (new ResultAuto(52034278, 'https://gorno-altaysk.drom.ru/subaru/legacy/52034278.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(390000)
                ->setDromPrice(400000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setMileage('107962 км')
                ->setColor('бордовый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/wu50wA-sSHO6RJKSgMi5QXknBSeWDU3Wk7Nki6spbDWtaWUJKcXAdpAbk3h_u8d-o-8NPG8MGbPKufpsVKSIteEhBB4v.jpg',
                ]),
            (new ResultAuto(52019284, 'https://ekaterinburg.drom.ru/subaru/legacy/52019284.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(600000)
                ->setDromPrice(485000)
                ->setDealType('высокая цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 AT GX')
                ->setMileage('300000 км')
                ->setColor('черный')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/3BALFdOtwiP5o8bZe73xsNS3aGKAIRG1f1vXbGnIkkEScZS6eU6cetZGzI52Irgc--tuzeUp3ksEh-2O0JKyjFFzQVnu.jpg',
                    'https://s1.auto.drom.ru/photo/xzaSPscmFCcb5UqwvJFpvEMF_OLnFdW75EEvsXMzUcDCheQZ1ghrBkRYNVBANQDNL6auFv_SdejEjQBpX7Q9am9yCDEG.jpg',
                    'https://s1.auto.drom.ru/photo/MMMa9MSvp2KiBQnZDHjBkpuOgcuDJ-j871UEz9jsfoEh5k4Kii0AVKX6QwNMqi7eC_KoNg0DAwe2rb778MkJB-l3h7Kt.jpg',
                    'https://s1.auto.drom.ru/photo/trLTmiFgvw1uqgFlz6TC82eUCUpMDKD78RLSue87YnkKzBTXBE51xkZrk_nHTz6n5OFE3P5jFvRL-G-TC2QXnB28GpfJ.jpg',
                    'https://s1.auto.drom.ru/photo/-DnP2UdmJjhtEiBVylu2PIyARbff-PDSBzh4fzSAMuAwpIdx8V0nqxt25-1GV7GWWGlwOYAD1d-lQz8q1HUxVVBgK11F.jpg',
                    'https://s1.auto.drom.ru/photo/9mjJsjZkqerElf9Bq4-e0eFaAgMQHid9D2G2-t8BRj-I-ZppBRYoq2Zei70tRZNE0-ryHWuQQJWqMXKkp3jqaOGtJVOm.jpg',
                    'https://s1.auto.drom.ru/photo/YgO11FVIbS72KJ_gIpusmwY3HV43gtViBv7USIlTbn2aulQhZW9J76_SbmQrO52iIMKukqv2aGVyN-LK1oJuoYRUUD7W.jpg',
                ]),
            (new ResultAuto(48749187, 'https://tomsk.drom.ru/subaru/legacy/48749187.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(465000)
                ->setDromPrice(380000)
                ->setDealType('высокая цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 AT GX')
                ->setMileage('80000 км')
                ->setColor('зеленый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/-k-cJYh-zOpntS6xW02YhFgGfSW4rK8RoLPmeupf5R6yf_p12BmPpepRWdaHDO9UZZKtrgM0wLia9twVcdLy4vjgMoKU.jpg',
                    'https://s1.auto.drom.ru/photo/jwhxC0E5b-BNuTDcnN-NjTs__lYF9vz1yBaliQsMxU0ssXnQSfUlUHHuUIo0hNwKldKZ_Jr6SWZtMYr9ERNb_LS1iTuO.jpg',
                    'https://s1.auto.drom.ru/photo/0-eGMu9s6lF-ysbwenIllguDS368MPO9i9paF_ygdi_NPYVwh9gkCzXxH1l67oFATt5-tNjVF0mH2m_CbH-TGtBonSDO.jpg',
                    'https://s1.auto.drom.ru/photo/eDe2ba_mx0ePOBHJ3JxzvISXs8_Z17-RqJvpUoQ673OeIz5mnkFAe2upXp3Un9vH69vfC3_0wSZQ6o_m7nAWE_o01Udi.jpg',
                    'https://s1.auto.drom.ru/photo/nUzGsQVXGVfpQQTOe61pqlywGXDd-UAQglUbpYhr4qfhkEErFFSQHSwgzB3MuKAuXMt0CeEtXPDzAQ21k316lZaTrGsQ.jpg',
                    'https://s1.auto.drom.ru/photo/rLjryYernsa4H8RuYCq957FBJv99ZfmiO7wbFfzJhwb20IQIXam9Kl2D6qOqDqisS-Hl_TaxOiD_OvpBba9KIUfhqLPd.jpg',
                    'https://s1.auto.drom.ru/photo/UHXHK6a3s1_CSlirF2DAYYLu9A9d7fcvmBVoLDqP3dnlUSCBjPrqZ4y3p4I7br-BLY5w_xXJ2bxcC1a_d3D8okjbQrEk.jpg',
                    'https://s1.auto.drom.ru/photo/-1JKEtx-UaTn-bTwnUldEADCGwo7E3lyFJ2N2Zpg91Quxdb_nH_HphmqQ9RdZeiVI18RwTXnV7EVm5BKR45kemL4i6UW.jpg',
                    'https://s1.auto.drom.ru/photo/YRQiaCMr5cc_c-NHtScyzfVI-5FCsgD7YOW0Hc4gjVFO0xwmS_QQ0Iwn5kRR33tLi_OC91nrbhZGVzSb5q2H33YgxED1.jpg',
                    'https://s1.auto.drom.ru/photo/0m7K6esRULKty8Ut64ho8CpKjDbXfEK3zml_Ca7Vv2iYab4yQf_VdLjn0vEB95qiOrrVACTkJWBCgGOfYRR6Oh22TNvd.jpg',
                    'https://s1.auto.drom.ru/photo/UFwikpipsFHyNE71Gdw0GK6Yv2VAdIAkT2am0r1K06aruu0dcOJwDH6SWUJe50aUUG97YlK12d8QpKlR8Ta8oixWfxp3.jpg',
                    'https://s1.auto.drom.ru/photo/qdFaDXQwDDFOcxfEOy4xk3NFS0-L51A-Bagys-g_VdwRuPYubW8ATk1Qn7VtZeRY7fGJs_bGoIZ94JRrT4xyLp6uZd76.jpg',
                    'https://s1.auto.drom.ru/photo/2-inpTKE_2CGVlTPPFgC3vXdV9UtuhRDJPDjf6tXdVGOoScrVQQucyVdtUEH0KFpQI62apkJz74Ze9P1X10G428H1ltD.jpg',
                    'https://s1.auto.drom.ru/photo/-8fzT7ILxFHjE_N0JeCHU5UerwEMRVpBx0BAJ2Lh0X0sM0TWB_tKYlMArU_DJaz2Gn6A1lHUduHSq-Wmwe47xh2MIzcM.jpg',
                    'https://s1.auto.drom.ru/photo/6F1y0oE4CRC5tJbfGkuDuuxSm2C3uvLiQ1cW7oWfIzXXaEkbb86FnTfaLpA-kd00o-2abGLaKSb2pxQ2exiZZ8n9RGev.jpg',
                    'https://s1.auto.drom.ru/photo/X-3kwyFJEMVXGa5NHaRKyaDF0cSt_hzzO723HcI1vQbbM7VbHmoy7JOZ-VfF4kMmcpmSdjxvpO8VIQaHqCsRCzzwOUZp.jpg',
                ]),
            (new ResultAuto(52002166, 'https://omsk.drom.ru/subaru/legacy/52002166.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(360000)
                ->setGeneration('3 поколение')
                ->setComplectation('2.0 MT GL')
                ->setMileage('150000 км')
                ->setColor('серебристый')
                ->setFrameType('Седан')
                ->setPower('125 л.с.')
                ->setFuel('бензин')
                ->setVolume('2 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/g5O_EopbU79b6KG3-njncVJ6alXZmPmF1mAo3qIttq6xbwNp3zm8nvm8Na97hpo0XKyuwCho91lk65kWWFh3HLPT0Sob.jpg',
                    'https://s1.auto.drom.ru/photo/4qPgexGD6H-CboigLwhBhqelEeGxHAVNVpNfRJkRzH1OB3nHJTuNJIVq-cmGRg-j3nZHm3A_jeVRZ8tCFaqCZjzz7EMf.jpg',
                    'https://s1.auto.drom.ru/photo/E5awpv3mQvgLSNo733tECEIBdQl9nCykXvkNFGimVTkLLuTelzW0qdSAmABa6aqR9A4lVOhoL4LbXEp5xTnrK9xzljTf.jpg',
                    'https://s1.auto.drom.ru/photo/le4n0SOCq3dGUb5XRCGsLVTxHnlKuMdxNkrL7t9PchCMf6gDLoCpwx0lm7HHKB37zSpGpzLjah0E_-jOAySLtnu0539H.jpg',
                    'https://s1.auto.drom.ru/photo/ha_4CEa4BYndu-xOl4RIZT3SV1FhLTa1OGt2_hQc3OPvK_hNCrtMsGKk5g2zmXc2wCJaidQvqZcmplAE2PdyYUtNRdzY.jpg',
                    'https://s1.auto.drom.ru/photo/OxJsUfez2JXMx9Df_eA2AQww55RZlVRba_pQEOq83WGyGYPmXBjhjCVGhnPPHcyDSkKosBfa2Hy5NrjI25EV2pm2bTfJ.jpg',
                    'https://s1.auto.drom.ru/photo/KXCCadNwBk86UJRvRTnSoDXYAVIigw6go5T-2BCvSmBUoRZgNFDY7S_iJjbYkE8fHhoh3zjyow1iuhUaSulwrVDqWd36.jpg',
                    'https://s1.auto.drom.ru/photo/Fp9cQxY1ooV5P-rVAoXacT3GToeU22ElkIOKZFRCtzgvq332ZtvoACe6YMJ5EuDwCwAlzHUtsTNV6yDVTOSFG_YXuO1j.jpg',
                    'https://s1.auto.drom.ru/photo/pTGZ9JJFA0N8lOXRV5MGzK3hzSNX7jnW4--EK1vj-eKr6f_8KmlQaH_g2_n_Yz0thZsk79jIO4RTImuU668bMLMQqNNx.jpg',
                    'https://s1.auto.drom.ru/photo/AD6vDzr1Me0Freez_9QL_ZWxZsVrzihFAdzNKlGxy6gc1I65fqg5NwMX-JpS6M-CUjPtOAMMQ_Ve7BWlmKsvWll0eESI.jpg',
                    'https://s1.auto.drom.ru/photo/H9m59I55NTqYgKNmmG2r8PyZNwzbyN36HYpB8-TasysT3XLF1xtemOQWTIBna0HuUkDhwbTAU3iwcV9YVCuASdyWKqS4.jpg',
                    'https://s1.auto.drom.ru/photo/q97r1zTSzjAT3Gmy80PVQ8phJRuVOsYzURCOD4YSdAavdEAGJ7PBhq6vKIBFEfs6LTS6g6raghXHWm1DWnMIBthbUvy8.jpg',
                    'https://s1.auto.drom.ru/photo/0DCD94BqBYvylG2zBphhMaFmDfR-P5J-KGjwETkyXP7M8jLhz_6OworQbEobE5sqwQvq4r_y1Vb7U71qbbvAy4Z8XOGp.jpg',
                    'https://s1.auto.drom.ru/photo/k7QYwCRdvcgv6Hm85LCP-QiHgEF86lpE1cg5-HL2b7NyIvfPg6qhRxo5JYmk7phmw4XDU30BJI3ODg4ImeVeLuwifrR1.jpg',
                    'https://s1.auto.drom.ru/photo/trwKAdReGs_ZzXcK0hFcjkhNZIVBUaEGusu9bD7aQ-10U5GGUwT3Gj8PalX5UD1YVuIbvxlx2pX-gKKwhGm8QcBm-VDs.jpg',
                ]),
            (new ResultAuto(50033765, 'https://orenburg.drom.ru/subaru/legacy/50033765.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(299000)
                ->setDromPrice(420000)
                ->setDealType('без оценки')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 MT GX')
                ->setMileage('380000 км')
                ->setColor('белый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setIsGbo('ГБО')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/cfMLg4uAKnhkrsUYVJjeM-bI-Y3Gou35i2FiFlHKMQwNxduw77B17yjRS5UbXCGQQ8MukZWzTBH_TMs8vH_5cVcjOxi8.jpg',
                    'https://s1.auto.drom.ru/photo/R_2rr1vL_-GbREUGnSzZM1VoLEJCU3ckw8JmecbFy2siRH98CYTP5uBihn9pS-rwINHGPeeyXgVq2lCyqAlS32Fcm54F.jpg',
                    'https://s1.auto.drom.ru/photo/fQXsP-4RjtkiYOaYi5z-m0k7BXV7rg5zCJC2UvaevyCueEz60NmeAZOf2V9eu_UHSmUg19PZLx9krCPp7XpJVZvfvr6v.jpg',
                    'https://s1.auto.drom.ru/photo/e6KKjTjCfGgEC6RDv6p4rbxe91-6wThPk3BoH4utff-0ing9LXJMRGDINgoowPR2beI07icyO_loK1BVyCPlCLg6XJRk.jpg',
                    'https://s1.auto.drom.ru/photo/1Cp3sbhdWVmVBALcvhJ5tazFuJ5STHPuzM4mSFnQee1mCZ73uIfakNVtYf28EPOm7iMM-aeWtbheOe2b-cZ6PJlGLik0.jpg',
                ]),
            (new ResultAuto(51951553, 'https://krasnodar.drom.ru/subaru/legacy/51951553.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(360000)
                ->setDromPrice(385000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setMileage('320000 км')
                ->setColor('серый')
                ->setFrameType('Универсал')
                ->setPower('125 л.с.')
                ->setFuel('бензин')
                ->setVolume('2 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/ccdqG99kDyVYuIOrjcM6YibrTE7_muAL2ga53x_gULCvBH89CMC-wNeeSnVkp4nUWf2vpQ8MM2CuQ0q5frHOGKUyDLRy.jpg',
                    'https://s1.auto.drom.ru/photo/OH6gibfIjYjB4BtgYnhgX-VGiKmIkbK-Omrm2KIXC9ZRYrn1BVnb8CkHIDcHuG4H0cXoJL684Qz4dl8jTktdRvA-d4zR.jpg',
                    'https://s1.auto.drom.ru/photo/vGVUYXO1-T7xJOM2C_-hGI5jtrg7QLIP0cHsgdDv9sCHJxa9BtsOxcU9tJEXhJB68DemMOUs2_hhy0KJasNrfilvvqPu.jpg',
                    'https://s1.auto.drom.ru/photo/brIPca4nofxrBAG90Og4xbKPFFvquE3MK89IxAJZFbDi5NAw8MEF9Y1INbgsGfsAykQJV-ZdpbzG1dlb9YXPCH3J6OYt.jpg',
                    'https://s1.auto.drom.ru/photo/BjbEPhwB2E2pMX83yfTCq7Lguo6fk9iomwiEhyAiDJQ1SLEhilLH-C8TK-LuJ6vBdMEB5l7fp2HJUwRp1RW3dxDhM88P.jpg',
                    'https://s1.auto.drom.ru/photo/bnqC7_CPl3uYE3ts05QgNvr5uCa6hZmJL_A2HrU_qCHEL4KJMLdS2JrrD-2aq0CSzVdGpZZIASe461U72TeSExzSYpR_.jpg',
                    'https://s1.auto.drom.ru/photo/jY-qnMy0jGDa0s3hp18pB1YlWOstgXt8_cKFLybYNmj1zIn0fQ2_mQh0OYohkqywybPEzOdOvrdGZoMzleXwKgDaRdBx.jpg',
                    'https://s1.auto.drom.ru/photo/qVmNkkXqeL3ssPdz-P1ZwbDW-HNZElHg_7aOvpUq3TsN272IVaqsDK_WHFVyqEDsnqoPjOGY1vLBKl6HWPESAgAj15wL.jpg',
                    'https://s1.auto.drom.ru/photo/xwil7GvYvw-vk7yLBrSIdfKTktJ70ksjnL3n_JHvL3M-gdbBiOqJbPm4xQbBsik1mkT_jP-WWumMGjJnV2YK6i0Vpe3V.jpg',
                    'https://s1.auto.drom.ru/photo/8zPMAySUpSfuJdfQmq_PNHZXlSprTdh2AsKwTtGfiLJqr054fKLWRjBsSdJmtOvI9umjMjdx5rbOw40VylPDYFvZh2j1.jpg',
                    'https://s1.auto.drom.ru/photo/lgJ_kSJInQIvyGt8Aetupj5APcuo6esuzcUhBRShoSgT_bs0xILPoRJiHkSYNPraLge20L6XYAeezUX1XAz8lpQldU8C.jpg',
                    'https://s1.auto.drom.ru/photo/hOoDxPHDoj8XmqqeBxYxRkTN8vVjglKxLRn1lW2_XlW4QDPiNy4SITmGvIDWG1FghdhnuSe-sc5MnO7TJR7v3ab6a1Kk.jpg',
                    'https://s1.auto.drom.ru/photo/fDpaSfGOM5mnLTRqFWHMz84hME5B9mLUzR9pZ0LUm0w9tIIjhbhEEmnHIuKh0Y9vjwaKS3-j5K4jpC9EkeMPC9xP2hy1.jpg',
                    'https://s1.auto.drom.ru/photo/uD5oD20GuR2Nv3WNCbz_dg0Q6aWF4tO6Obbcu52_Qb6EjuOyl-g6nBMNQu7se1rm0IJc_93aLBcHmPcmjEMRR6VNrszN.jpg',
                    'https://s1.auto.drom.ru/photo/pce3TUDSM5cZfJzZs3dexKPMtmAnYsXZka_YaBjEJm5-jDQK7SR7GLtJdwaKJc-ryZ_jixuO1CeT5QlTCXuPiZ48byud.jpg',
                    'https://s1.auto.drom.ru/photo/5c0rEQN84vFnZ6R-xQto6pfKRwVpmnttOT8cCUrKPU9VOjKTihKx2F309jz6E4vh1JnyPaRBC2y6G6-O_WL-OWVqNpeM.jpg',
                    'https://s1.auto.drom.ru/photo/Yy8pLFZXo1Xu1falvUFuASYmMXd0hWvUK1jJZrshsDoBY3kX62EiPXL9SZ-okCiIVSkbG1PpNErCMU4BTifb__7rx-u8.jpg',
                    'https://s1.auto.drom.ru/photo/18auopCabX12W_Mc5fd6iW_8gimZ0BkymKY-tEHFbq5CiNAUYReGw96FUg3sX310Al8bPd8RpT2hpTIRb5qHt7S6R7gh.jpg',
                    'https://s1.auto.drom.ru/photo/pP5eRmG6xJVVhQ-3Fyd7GYgu5ynPxWFOFQ6UgnTnaBUP4DbBvotPPR36C8A5YjpjZvu1sv6LL536DPaoK0lqcaD-ajyt.jpg',
                    'https://s1.auto.drom.ru/photo/VfL_04arDk3JehAgESALzlLpS7NVd1L2-24DdsiX_FfNqZRFxeZYq4lARSr3h46s98jlqYZxFpHIb3Pv6VknJSFz2Mfp.jpg',
                    'https://s1.auto.drom.ru/photo/hPMnr-V5JLL7f45hEvVkw9aRWQtXs3HwkpZQORoslKHFnm-at1iWX7KDAlOA8Hd1fEWN9cmrMU_kSpyOFSwfpY6g8Z3Z.jpg',
                ]),
            (new ResultAuto(50355504, 'https://krasnodar.drom.ru/subaru/legacy/50355504.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(325000)
                ->setDromPrice(365000)
                ->setDealType('хорошая цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 AT GX')
                ->setMileage('234567 км')
                ->setColor('серый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/JFAYdrqEUNI3oLEVpaoZDdhqJmP--mUzQU55OLwarqEJ0qZG3GLdfKRQDA3a1APeI6QOduTjKKsm53WHf0TqlEO45Ma_.jpg',
                    'https://s1.auto.drom.ru/photo/6LF9iI4_DvpbW7wuR1AUgLe8nf_88yD77nrFIIbL8JLp3WZF_uzrGHwCwaZO3pZVsFTNe-Dn58uaBAa0U9BMzNZ7R2rD.jpg',
                    'https://s1.auto.drom.ru/photo/PRuJ0IVOxOYgeDq1OT7_OfI1tLdxKfXP0Ej4McdmrZrJz14i9ElXLFBd5eiDUKcz2on9Ulj_BmB3pE3n0zWgISfvnCA2.jpg',
                    'https://s1.auto.drom.ru/photo/adRrHhAKYbYk1u0YGnZTayAoocuJEBsls1SsZVqJu5dhoOXvdstw0CWNeGHhtaiYrOIKQbFQVr40ikU6O3H2FCA6vHQe.jpg',
                    'https://s1.auto.drom.ru/photo/oPMY5ZuCp48YOy1ppZdDOfEfU8B0L4iJPHJUkTX0Cd0wg1oebUtC9043tFPpx3vpY8X2R3Qq2aQfHwOZyX4KSLQQfYQ8.jpg',
                    'https://s1.auto.drom.ru/photo/jYCw--YCUe2NqmsBX1kOde9eWeer9RvHKr_JBF18yV45oBTUM_KM8OrX9lJqhuvvdhyDKuswHgucxEfYQGlz7H-aKyqb.jpg',
                    'https://s1.auto.drom.ru/photo/EY7UfkteAkBCVbogzV9B2-27h5uU3jPKmy8xaCqaVEm9Uuyoyfe0XqUuamy4vglPhs-jksgIefJL3fdbwlaXoFJkCBhI.jpg',
                    'https://s1.auto.drom.ru/photo/rrpiHil-iKT0e4tXRiLExQKHTcWaciILATwpIRRo7TL9slcb3nBy958J57h_6fQuEYVkQdd8R5Qo6EaJSOShRwr_0c5z.jpg',
                    'https://s1.auto.drom.ru/photo/lCQVUmEOgKq_QhOWnFaFbTQkik-_uOjRS0nBP2cZC99ZNwtWrlMuolplscT8pFTzJSN2nnx2R585DjN5OXexRNNmvaUB.jpg',
                    'https://s1.auto.drom.ru/photo/tD7X9tMbinzQoHSHTvPe83ZKt3MwNVHkC6etus8Ackmsr2y_fleXdqRtLQAhnOxIZQF0VrrdhdHkG3E8sVRoS8m3Gv1D.jpg',
                    'https://s1.auto.drom.ru/photo/itU6-CKiiyTkikN79k6wuNcy4OHhhAOgwS9I0TjHYSkl7DtXGaDSrxrumNWjpWvhXuzJiKZpmHq9JPn4oKK9kRKAuDAQ.jpg',
                ]),
            (new ResultAuto(50384081, 'https://gorno-altaysk.drom.ru/subaru/legacy/50384081.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(299999)
                ->setDromPrice(355000)
                ->setDealType('хорошая цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 AT GX')
                ->setMileage('340000 км')
                ->setColor('зеленый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/E8d05fhFOdtSDcLSTwtR5khRX-TmvC3D6ETw0z1BKYJ9qJGTekBqY8vQ3rADHQCFxUoV67lDIIIkTiRuFkJ7LZxZCg-g.jpg',
                    'https://s1.auto.drom.ru/photo/sc2d0RFmzRHoew2BB0cbh4-pYvvP3AZRx6fzMndNXAm0Wm54W0Y8KZVAL4fSOPR2wR5_oQ5x2vWOtNlzJOpiH1zhuOqc.jpg',
                    'https://s1.auto.drom.ru/photo/PaWBaKQgFP088aTvwbmhpCOCArBOwZ89gMJ-jC1cy4wfQH1aZ94MJ8F3Xjnr9g9-i0cGSuG9Hu9Il4j7Yxbeo4ldOGal.jpg',
                    'https://s1.auto.drom.ru/photo/vuy-nHOgBB8TDCMEX_k1qDTRoyOAYmMilKDeaAOyWXi1RP_4zv1aTMSbDlvaPi7Dl4k_M6XqzXVrFam48CBrNhfbAudl.jpg',
                    'https://s1.auto.drom.ru/photo/hTEFT0Z1uMPdMxX9UM3tEQpElUtFVGlw19LXiH7_OFQYH_Yc8wcDVWWf5qZ_nhMx3qfiYlTEyEBXkFVcZtqM8yflBHH9.jpg',
                    'https://s1.auto.drom.ru/photo/3XymGvoi_TlhZFcpYdCQNjbh0gLUtwiyNX9VcTihfVW3sBaNS3gO3q2Y_xi5CGTyIOhuNYZmnR2REQpAwvF8OgblZw5N.jpg',
                    'https://s1.auto.drom.ru/photo/Z7WgkmoBGLsmgr9iGBcd4MHqdZckiVBVj2przK6bHzLetNQqnO6nUV-9GZtkZwo7cmrXFdyDnFqSfdXMLFEJIBA_fFa0.jpg',
                    'https://s1.auto.drom.ru/photo/QRM3HHDTLJHakMv9cLn0WLBSktYcmFVNQM6L5WqHHWaZvbgfi1v3tLjYWr5oNsNqKPLaaZhFNEwVvSgfeAl5JCTPBMEg.jpg',
                    'https://s1.auto.drom.ru/photo/AWXdV7L0ixDSLHQk-Z53BCnLYnky5japT8s95txCaqgHyNybAsDtGHEIBQhXvN4yaeHbToDMhY5-Oie7f4BPrygL8xbl.jpg',
                    'https://s1.auto.drom.ru/photo/CszGaRJd6kB684BzSSNI9SaxEeC7bEvuBjwevcMu-eyOjwlp00DcW_FUubqKiGqfERuwfUauydzEK3YHOWR5_xP3Doue.jpg',
                    'https://s1.auto.drom.ru/photo/4VhB4D3txZt894RJPz_SFYAQrYHrSufHWp6BxbG56gyG4IyNYctVFWaNJlL2h58T3VSYhPBjbjNKd75Bb1zTGrBLntaw.jpg',
                    'https://s1.auto.drom.ru/photo/3ENNNsOkrsahvH1jyzHyoJLA_rKHTH9wWonYRrEVfpEnkdVRIXAlQ3tMwNHmsf_0PcT7fN9hLURYsLnLLdnEkrZAl1sH.jpg',
                    'https://s1.auto.drom.ru/photo/VQ8_vPyEnptTSJd-gDXyQlUbeQP90vVA-RCCjtwjsPzkdb_G4c70GSs5Ss-tt9zFQ2xC9CvVaeYbUO16Eb2NusshnmYP.jpg',
                ]),
            (new ResultAuto(51925599, 'https://novosibirsk.drom.ru/subaru/legacy/51925599.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(250000)
                ->setDromPrice(380000)
                ->setDealType('без оценки')
                ->setGeneration('3 поколение')
                ->setMileage('420000 км')
                ->setColor('синий')
                ->setFrameType('Универсал')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л'),
            (new ResultAuto(49742241, 'https://ust-kan.drom.ru/subaru/legacy/49742241.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(410000)
                ->setDromPrice(405000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 MT GX')
                ->setMileage('19200 км')
                ->setColor('серый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/b6jCcBUqEG1Tb3_R6KGx5RHYP9qpi-1d3BVVWo9k_yDn0bvO5OpnUXWJtS5bdhHnmrBaa4DoBhPI-TPL2MEEAOzCpsKe.jpg',
                    'https://s1.auto.drom.ru/photo/588BJhfhV6J8TX58PpRrlp-KAEQUbunSeck4u4eoCnWzYIyhSUXH3TNd7jUdzq-TMacrXnIrUgyjn8kmYwVYH8OCT9NK.jpg',
                    'https://s1.auto.drom.ru/photo/oiyj7Y4Tu5BQB3Qi1xYWLXltqc5d-YilKIDoPfk0pwJQDYg66_E_Uh9ZupDjX9-2fAXzjkfhwcuWw4-ZRlKs5JCfYYwZ.jpg',
                ]),
            (new ResultAuto(48687404, 'https://borodino.drom.ru/subaru/legacy/48687404.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(405000)
                ->setDromPrice(360000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 AT GX')
                ->setMileage('470000 км')
                ->setColor('синий')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/uNWmWvDO9Sfmjq3q5d8ZTJfGO-ciZeRm10ZFmkscq_tB7JjiZlIj4nszctY4IbLqR3fDUS6Ft4PCy4_YxePVKBwg3RKH.jpg',
                    'https://s1.auto.drom.ru/photo/iGfmfwKQ6b0lIatR_661et2CZTT0sz8EGnOCC9XVtpcqrnoI3exGT96_k4ureyXLyfE2VPvm_CGlxmU0qeVoK1q0jy1S.jpg',
                    'https://s1.auto.drom.ru/photo/XreHe_VQXMdlO2Kz86-ICOZXHQCMSfup49sw4piBTSrYTrab1fWsb29z2QrHGfBi39odDUWeNCADhST5lVcESH74VnJ-.jpg',
                    'https://s1.auto.drom.ru/photo/-DzkCfApZdNLPoDVNmnznzxmBvFgnMIO1J2EuNs4i37jxGpd4EeAkVbReVTAyg9IitSSJd362qhmboT4N8o-rMWGilY_.jpg',
                    'https://s1.auto.drom.ru/photo/6T4IaAM8Frw5A-d8GDHk9bpDx-GtL1ooKeUou7RykTHsgVvpo5pU-80S89_-_8hIQmdH11jqibjt-2t88_UnqQPQFFrg.jpg',
                    'https://s1.auto.drom.ru/photo/pPuwt5wlz49jSZMZ-K_7OR_a7WeE4GzZZnfiDe4ntNIYMBAaE_aKz4i9N47EQtEhEK-bXAOB68bhID7WV97zarMXsDu9.jpg',
                    'https://s1.auto.drom.ru/photo/PbU2skRIV-Qg1NDP5YPAWOh8KQmf7CWl_QRHR2PJlPfPbbY_CX2oxoso1uwDC2epvg_dNBf3wkyNG9EB8exY0krdAC20.jpg',
                    'https://s1.auto.drom.ru/photo/Edj1uJvJXuF9PoATZYynJ8yAJcoLsqV5ldBkKIx4_yAoCiCSofXa3I4pW420xM2Kmm-WeAnpk6ki0ibLTeYV02AIykw6.jpg',
                    'https://s1.auto.drom.ru/photo/dqAUUg-s0A1fe0amYGoxDqxCP0CSpfJcoqehPFkQQLUsdvpo7For8Os_wedK62nZAS3iXzLstTnLX7Of97NKF-RUXjpp.jpg',
                    'https://s1.auto.drom.ru/photo/QdbHlPQUngHQ-qHItL8F5wb3LStCaQiiHxqher1C2G9H3lPRKvZgfTltEtUcf09o10KYKMGW07by-nlJ7DITKajTaJUA.jpg',
                    'https://s1.auto.drom.ru/photo/fQQMkuLjyhpFO7oRUrBgXkwgFWYaFpu7ODtxoxA-bK7E5bawu0TJhvRfqQnSNZOqb6Ec0OYG0RoW4yvhGz4vaJifQs9C.jpg',
                    'https://s1.auto.drom.ru/photo/0YunNH2GWs9mqGLjTnGv9tT-dDPQmZ3o65F5NRfWyS1VZtLw5shfkPMIPbW80QgeMVXcvC4FiMqk-_CPqGib5nen9mYp.jpg',
                    'https://s1.auto.drom.ru/photo/gN-jU_oXNsAJghRFmQzFZDlXooZCGDcycomVqjrgVJIFBuPcb9UEoJU2-rLnASFMa8ZEbOG9I-8PMI8N7WTT3T_NglLr.jpg',
                ]),
            (new ResultAuto(51622548, 'https://novosibirsk.drom.ru/subaru/legacy/51622548.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(500000)
                ->setDromPrice(365000)
                ->setDealType('высокая цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.0 AT GL')
                ->setMileage('100000 км')
                ->setColor('синий')
                ->setFrameType('Универсал')
                ->setPower('125 л.с.')
                ->setFuel('бензин')
                ->setVolume('2 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/1MnDsZpoCMuyPc0zTVQgDELBpcIyOOlc4J9zuB7AofixW1KEioHQcY2I12QxwDyO1gmSlfmvkWZ90GFtycEZPrOY9af3.jpg',
                    'https://s1.auto.drom.ru/photo/Kehcv6NUZuYyKc6mdNwx5UM8mBwHodzFkv6nZm40iLj2qZG9w_Va4RsO5tLE_FPAF0OZlx1DNfP-z30-uPDy6jyECD-8.jpg',
                    'https://s1.auto.drom.ru/photo/7Q6PoS5Gis8LHosBgS-O7Nk3_uOFtsGXVWWnPgI4lXB2ju_AM9Dp7TqzsVanVsIlykd_ZgVJ8F4dVomQac6zSAWuXUM4.jpg',
                    'https://s1.auto.drom.ru/photo/14l3Dr7UotRqfvwF9S-FRtLlf7GzyRZBmAaPucWskY9CjT0aEMxBTSeYBTlFxz8F_Dk05lY9-CZ8GM6jEn5rS9Q0_uZi.jpg',
                    'https://s1.auto.drom.ru/photo/CYOUpKb_rcs2n1pi_Gr1I_or3IN9L5saVtTFsWYLH2nrT1aAUS1HSUx2QkRFE1ZGaclXjchs1377jASRbEdMMTn6IS2i.jpg',
                    'https://s1.auto.drom.ru/photo/1djrjaOHkyJ1E5tUt4nog0Ri5MCt2SaVxm1iZOSz0Wr4pk7_2vhZMC6bR4stiuL1nYaWISNKFTfAHmOrUFEBlmtXILJ0.jpg',
                    'https://s1.auto.drom.ru/photo/QkYhcMYKweC-i1c5EYdvqlTCIbNbFkro1MsdNu8-ub1HA8lw9t_qCbxbR_awGk6km4I6Zm5sqNeemGJ20aA4V9KsWhAV.jpg',
                    'https://s1.auto.drom.ru/photo/pfZJUvNBWDyKmMJXz9gRDCxP_igwue18G7-IprYFr4VYZby9voqtqMiDbu-lHxValV3QNIStZHXF8B6UOWUZ2j90YNRg.jpg',
                    'https://s1.auto.drom.ru/photo/sGC0B6D2Qk3v9A5CeJErW3hjfO0sVRk-LfkLpg7FKLkslmcODcGRct22PmkVnLM80KFA4gyVr6ggZiHjITty6MwvQRoy.jpg',
                    'https://s1.auto.drom.ru/photo/VkzhyXbGbLkRagejEcsh-7i7Jc3v9oWfmvr7k_C77wHfXbLNzjWcSuFGEZZualSNJqlNlEmhhxHnSBIkysgPRE9bxwvS.jpg',
                    'https://s1.auto.drom.ru/photo/l9ErKkQDLonaMfNjhNBoTdOQV4XsK9K2o44wAE0LGoD0ONeLNR9q3Ak7kRJDp7aveqlwEuPvXdz7yp8mjGtXAUycDBdQ.jpg',
                    'https://s1.auto.drom.ru/photo/QTHccmC10SCtZEmntD2guhVE-DpqaBt4M1dw5aSEjGWuYwrfeaM2jHZfSIhSfeZmPONC2NIcCMi6tAthdNi724cX4u8K.jpg',
                    'https://s1.auto.drom.ru/photo/i3nbeb3NWV25a0uUO6gbbYubzhXwojwG1Tfm0XonYzltcKSpV_2Iwdk24DtycoTG0ocNRYKSEXMNMJAhbcvYDN5RzTSs.jpg',
                    'https://s1.auto.drom.ru/photo/nkI5mSE8wJV73j5KdtbCMd_DUYAq-zs0RJoU3ydKCj0DJ13-vsDwXHIe4qYJpBdycUIh3UiBXNWlV3m6FfauMpeifn3E.jpg',
                    'https://s1.auto.drom.ru/photo/DQWBVcgX_11gFdZUqYGUC0y_XAIImJlsASP3DhM0O4ii8rqB1vhCMu-s5AMxMSfbwyv2xalAxct9NK_NgnUuEo_Z4pkf.jpg',
                    'https://s1.auto.drom.ru/photo/Obi7M8JwX9KpFT9YgoWnz61qnz40ego-ubQE1K_sEyF3cyC0cpwJj6P1CpbruyK1Elty4RTBX9CM6ZdDjH6qx3EyCyFg.jpg',
                    'https://s1.auto.drom.ru/photo/N5iz5R5hq7xkBsWkDHbatcdXJMn9upC1L0AOnnRGdi5KTVtOHNjGNuq0xASEYrZrdv8gSIu3jFymoi4iFOE9R1FGJ19i.jpg',
                    'https://s1.auto.drom.ru/photo/_HH7vNjhK76pjvwSSLr31EKvLEORsSizneOWGbcFbqAg6YQ7X2NQy2N6TCIH7mCgW57c-yHH8Moa-roBahsn4gcH1vXk.jpg',
                    'https://s1.auto.drom.ru/photo/ZG6jecJDbvyMGUKFAtLAOmbAYgHeX8uXTuE-Isa6FMYLGsdtzoD3ksq0DC8tvEgXYTnxlcQMLEMCnCQjLvn_OXEr-gkG.jpg',
                    'https://s1.auto.drom.ru/photo/hAwpjkbTOy6X390s_CAO9ZG-rbnFqTZf_49kDyNzzajNfXncUX863MbTqb-G1NRuvNhBbMz3mn-lOUs3lIXKUUMpiPY5.jpg',
                    'https://s1.auto.drom.ru/photo/A4u7q1128Pih11FK0s54mgsEYWl94zZFUb1CUXvkjL1w0m2Rays-qLFQjcrQLsl01_XISB3FqB_u2hFc08WTRRq7WfTs.jpg',
                ]),
            (new ResultAuto(43155545, 'https://irkutsk.drom.ru/subaru/legacy/43155545.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(220000)
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 AT GX')
                ->setMileage('25000 км')
                ->setColor('белый')
                ->setFrameType('Универсал')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/6f2V6vguGJD334MxPu8Xg3QcMjXoJyrj-hhlAKWAfKZuS7z64SXdOME_5zo4oQTD17KJPg_uvajtMZc62gKDLttrvi-o.jpg',
                    'https://s1.auto.drom.ru/photo/sQZkxJ5VOlM27TUmldYBVVJqxCVelcriWYazMsJQ2JYF3zlvlcwQFBeZzv6bKjY20P9QH33XAwnadFac6Z6J2UdJr9GA.jpg',
                    'https://s1.auto.drom.ru/photo/DOm5visXw4Xi5oZBY9I_y8P6T6K9Fudng_Ukt7S3XK7fq5BCCYcTobxdkQGq_IWRIiVFMjI39c_wNATkoEqqU5_rwfdT.jpg',
                    'https://s1.auto.drom.ru/photo/f7geqsyzaWfUE9Kxpk-tPSaAjPk2t3TFHst3Rt77782O_luhr2eI7IPuAkIX61e6JcS7b60lEL4iq0BeD00ASnfOKPWL.jpg',
                ]),
            (new ResultAuto(51464779, 'https://novosibirsk.drom.ru/subaru/legacy/51464779.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(500000)
                ->setDromPrice(445000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setMileage('200000 км')
                ->setColor('серебристый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/2SHqlCckuxNbzaZLdadlKhNp_kZ-6Yt42I8-eWDvdsyYVCN62KkhYhi77xwMBnAAcB3zoyv-pfiYyIe5tllGdksn7KpC.jpg',
                    'https://s1.auto.drom.ru/photo/HwHKuU-TU1Ye2-lye02t14DE_1KQd-gylnxiD_ZJuvVik-sp8IXjz4pIdKUR5uyUYUYkG91Xb6Q6VKK5qnTQdPHxV7hx.jpg',
                    'https://s1.auto.drom.ru/photo/F4e9aBdL-sZsHp_K09lWQeT4oY5ldObStwz6Liq1NyyoV-StYtafhuCFHpLPNlNYiK7qU5DyYuDM1KQcH-Oqlp3rMkof.jpg',
                    'https://s1.auto.drom.ru/photo/ypLkpMayNlKwZoU9qoDRXavGfIHyixJfeaYCyOnlk3ZfQWDl30JE0IH2yTMpNu-318OPcB_hcfR-SR1pldzqIyDzLCzu.jpg',
                    'https://s1.auto.drom.ru/photo/8V7sQeuMDLw_xKTaA8kc3hpBvgMX16t7xwZSiNYWnqPPUbQ1eUyqEurwkA6SK4APVrtZSVwM-1RhOdveyVeST0xOgG_R.jpg',
                    'https://s1.auto.drom.ru/photo/c0UpTIvKM_5Y3RPX6MntbBtqMCIS7ewTN0JpvFMaI13bUhAc9LtihJfy3PPhZSMgyliwL_VCfEsTpoosaW6LuTn00uRM.jpg',
                    'https://s1.auto.drom.ru/photo/lweEKo8Y2zFIApSRNEL_byn22B1V9NWTKOwt5mQnQ3zQ1qm1zRxG-eWDGzkuBBrdtVLQAK9LzfOhOIgCEi8R3zWWxU5b.jpg',
                ]),
            (new ResultAuto(51027014, 'https://voronezh.drom.ru/subaru/legacy_b4/51027014.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy B4')
                ->setPrice(430000)
                ->setDromPrice(390000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setMileage('170000 км')
                ->setColor('зеленый')
                ->setPower('125 л.с.')
                ->setFuel('бензин')
                ->setVolume('2 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/gdG9MxeCifT9yLRzXjIfYxD4NXvzshLCLHC9k5hSF2QqVAhKelj49L9VC72zLUnnAvC_oJrfmu6HWUAUQPcyG7qJ2rwK.jpg',
                    'https://s1.auto.drom.ru/photo/-zEDICfwW-C7kaai91c8NUW8EB88ho6YkMv7KqOw7P8APaHDHk5T0q-gROLo8ljBOMnQBhoVAr32y8rVy1lzrH8PbZlo.jpg',
                    'https://s1.auto.drom.ru/photo/vmVB8i8okSZ7ONtQS06YhpbhrPkya0djX9TJDcbzO7hxKh2h7dD158Vgdz3awfrRInoDkQGlIos1P0g7EujbFb8OgWMI.jpg',
                    'https://s1.auto.drom.ru/photo/BrpGMdhTax6hpeI3JCxwDAMqOIe9SKfbW851zdJra2av6E15Dh5UlR_wLnPvdTyzlZKeTR2VbOiquHCKhl1yUqfTAi4c.jpg',
                    'https://s1.auto.drom.ru/photo/neVJdO_G2BLZIpKk6Ze69Pf8MiR2SvxN4Bq-qWQvY2QlnkwJhM6Y26y0Dl8fI5F_03d6n5cF4N0yBkZzh3wuUpUTqyNK.jpg',
                    'https://s1.auto.drom.ru/photo/6OqybYMdRbb8rIP6Nbli6SF2L4pVjjVWJAYu3SWlLtrv-XLrryFdhk9Xil3yvl4IRCeEwk3NM9y7vC7rjp-GPmt5x_Mj.jpg',
                    'https://s1.auto.drom.ru/photo/bRx_N2CR0e-9uHyvuxLJ5uweniBlmNExBH2eFViHeLu-I0BTNIALu93P337_gYlnhuknKS86TM8nBoi5r-DJrMXHGrdJ.jpg',
                    'https://s1.auto.drom.ru/photo/Px6L3pxju5JRRe5orQHNn-4qK4GG_J2PRBDYTlpeVB3rcgT21BbxNINvvFmSCGcJVWfDaaGturNfaXPMgQP-oFAu2enG.jpg',
                    'https://s1.auto.drom.ru/photo/SOZAbmNdaF0kLfm6uRkXwlGrsoluJXnAA0YlzXSJgBIY8SoFyD7xhysoW5rNH3Rj-9Dlo7dzSuxtBPo4bX1ABgSGtu4a.jpg',
                    'https://s1.auto.drom.ru/photo/Gbp4sCxeDHdkO4rkCemkKN-5dWIRf8eutaRLY8-eSCVU1rOOKUAkuJowYgeFWRvqcKXPTLZ-Zhayjzhzu3BQlz7cPo8u.jpg',
                ]),
            (new ResultAuto(51776988, 'https://biysk.drom.ru/subaru/legacy/51776988.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(255000)
                ->setDromPrice(450000)
                ->setDealType('без оценки')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 MT GX')
                ->setMileage('280000 км')
                ->setColor('зеленый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/AhDSourKthI7RzecJTtvBdM8Ha0K63ax54H8N8I8Xw0Gbdc1ppkdVSNZxqeh-GA9xl4QWhdF0C0_ah1812TyZfmyp1vP.jpg',
                    'https://s1.auto.drom.ru/photo/CCrvJ7Bfrz42CqP-QHcCVoUXp6q7mQWtck3EZUXZUO9f-xIkTiv0wE9awbKiyHW29oL_AaRfrF8z0bfRI-vOz9Vx0rW1.jpg',
                    'https://s1.auto.drom.ru/photo/5PY5_iLN_X4KNzenaf_zRtb4Q_DOCG_KnNtKKh7L6yXeLmt80eyIqSa5a0gWa_4y7MF7Ry1xILGFRWaH8NjZm3H-M86f.jpg',
                    'https://s1.auto.drom.ru/photo/IEqIvaLSmHCBF1rV90cJAwBiU_bCfmt-LAJ-AMar39LhpOknOPfhbD7KZ2pVZhGKYHKR5gqyOUwdF_vO18aL5kpn4pMf.jpg',
                    'https://s1.auto.drom.ru/photo/GgJE6A-Tu6C-xo68WFpaxrndFCWLOwdThsllZQ5i7ZQuocaEsLrwV4hzD3R7S2nXXhBTdp9-sni3laPI1nwetSU_9kah.jpg',
                    'https://s1.auto.drom.ru/photo/08TVjNTj8yW7Y9R7re3zyzWcpI4RfkYSMAXhZBH4OXBZR2TNpFj3-se1t_bmmpt5t-0TDBG1TacGN2Hey2DXB7XhTPiA.jpg',
                    'https://s1.auto.drom.ru/photo/W73gr-PsVnaVoaR_wGpVtheWAByviL1J1M6i8fUCyv33Jc2XfFlaG5p8aPuMnfQ3YbIQpfwj9_IkhTsq12AmkozjEBxh.jpg',
                    'https://s1.auto.drom.ru/photo/axnUp2diwfCxLrnmRi-bpEvxpkzaUtphBbb5zK-U3B1-4gD1L2Y8cGY3CA4Iaqm2LZTyNFpOgv7UwII3kGMUpR-CP_5v.jpg',
                    'https://s1.auto.drom.ru/photo/ixdVLHjqmBMW4oLz1pUHvyKcTOwFtJ3QbY3BE4rFUXQwIiJTcih_qvgDmFZ06VJVS6TpK81dS0j_Cp1Ls-_MM4b91rN3.jpg',
                ]),
            (new ResultAuto(51755283, 'https://ulan-ude.drom.ru/subaru/legacy/51755283.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(445000)
                ->setDromPrice(410000)
                ->setDealType('нормальная цена')
                ->setGeneration('3 поколение')
                ->setMileage('414000 км')
                ->setColor('зеленый')
                ->setFrameType('Универсал')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/FKdK6C5FZumLdq_TWq96OZYtwNGT3ofNI_JgNL0yrJ-xE1JHyrxLBwowAxVfrIv1pj7XsYqQVmOwLOD1znuIz0rVAn7S.jpg',
                    'https://s1.auto.drom.ru/photo/BXyX4nsqduN7C8gPKvgPASPFoV8PR6Agb6ycfCG2H8yRbEMU2XjkvsNpvIyH1pog5i-7uqNYNcXUEX0xBc3Wm-81-Cz4.jpg',
                    'https://s1.auto.drom.ru/photo/DJJfA4FYMjkYVumInGm6wAT8c_r_AA19Udk3_9cEs3UKYL5jVQTfl9ptY0PNxhEd9yPf0R2JBKOL-ffCYXxQR49iPpso.jpg',
                    'https://s1.auto.drom.ru/photo/P1IOGxWZFgpmpXFqM46efHmvYbld4ImOGrlt7jLOP2_qXW6ixS2_u0zOO_CD-6m9UkuLAvJtCIyK7FalzlTVePIjbLew.jpg',
                    'https://s1.auto.drom.ru/photo/LkCr3b2s1OOrvH73yZaMlsddEElESMPJ0eVMFo7iWPpWiXsp7MtQeCwDj5uuFYurmSObjkh85XONdRe_0LcSnduCMgF1.jpg',
                    'https://s1.auto.drom.ru/photo/D6uvCg17NYBG-Us_grJwEQk7l2Lgmm6XrWBE2GhDHU7qSIavAmUQy60ZX6plgdpIpJvtfKHGYaGAfqmmGaUdMVbKIOV8.jpg',
                    'https://s1.auto.drom.ru/photo/vpK4Kpp044jePZobhGaHKLprxNS09omMzKYnma8O79cdMsZ73mKeBKWYstN8Pofpbgq7ln_LtbzUZmrkmAycuTeeI5BC.jpg',
                    'https://s1.auto.drom.ru/photo/p8amoKR6pmgh9VrRqwZ_4Z6l-o33ZfZo4SYLVNMsj-m1xfEnMsLU0zhxCdBImudIKyqhPh3EZMIya7wOKUgHRo-0wHFt.jpg',
                    'https://s1.auto.drom.ru/photo/NSpjfBF6spurvkDfjGndrvvPeEJf7CeXAB0xqOXPIccZITDun8r0pi1sTBPbxiaF5LBbVq-TnWONwh5phA-HJXM7T-17.jpg',
                    'https://s1.auto.drom.ru/photo/uLgDde-QYqrLbZsmhXmMDUYbue3jPoNpBVrYPEGSnwzrbzusioJUJCIYTRcl8MznhBh0TavpAKDzOUXPO4d2Ui0uMcW_.jpg',
                    'https://s1.auto.drom.ru/photo/CVRVIGjbIUFzLyvB_PhD0tc1kFCgjfZ1lg38i32tbqUkFgbfakDLtewa8OyjYDvPzD8aV4vO9cKxabRY3z-NiC1OQv1l.jpg',
                    'https://s1.auto.drom.ru/photo/S1ne-WDDzrKlVOb8k3gjOnlqgUgQRZ1s5daG561QkDRN5OFaoNk4hWkl3tEgQm9f0hipUM5U4ZvCWinnbkdtra3-nIIH.jpg',
                ]),
            (new ResultAuto(48887344, 'https://balashiha.drom.ru/subaru/legacy/48887344.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(735000)
                ->setDromPrice(495000)
                ->setDealType('без оценки')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 MT GX')
                ->setMileage('211000 км')
                ->setColor('серый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/AEtWSenxxQVBSfmIfmIdzOTGlw8Y9i5KXjTrGJdnSiNaVNKcbyyGaDT7bZgymcyfYN4EteIQI1Ku3SbcSLHvx_oleLqz.jpg',
                    'https://s1.auto.drom.ru/photo/4wqrM_-Wcc6hKBU1xdmp7HyWtwcssz689-JRB__jkrpxi_mBGqbw2uRyPt3Llemf23akYtAC4nUEdWqCi5hAZa-zbKQU.jpg',
                    'https://s1.auto.drom.ru/photo/Utj7_Mh7jgBq7xB88wZt7wxEuPedQZ2MXhLdxnVqhUTN-US86L8jYSyBLMd4URXksJQOvUgnvHH3NeR-m1Pq6qxuSmD5.jpg',
                    'https://s1.auto.drom.ru/photo/Usl5pqoRUyEd0o3hPPPpZs5lfXWAtns1Yc3PaEeHH24IyoLtBbYrzaURMw2t0Wx3ZYzm7SpvAqTbO9s_oF2Qxe--6V7c.jpg',
                    'https://s1.auto.drom.ru/photo/Yf6DRAWNOo3h3AE4c-nyHIVC-xGRnSpItWrmaOSLTZ9Z3_gVxkiTEoucnsfdqP4nmltG_l_QieI8G4XNyRwNBifWQV-y.jpg',
                    'https://s1.auto.drom.ru/photo/rqbqg-aGtg-PhR5HobNXnwhk4NExLNYMJHde_IKRTtQ3N0kZTm7L0Ur9CCspu8m4LKQBCisCWXTYRTKUmeSJkjaFlgab.jpg',
                    'https://s1.auto.drom.ru/photo/HO1o6jMZO6kBGKvxXvHTWf-088C_2kFtG5FKCzV9yPmrNWaQNIq5zAbjl_39PcY_DdcVdXr79KjKDXMoQrzdBHwdmLKo.jpg',
                    'https://s1.auto.drom.ru/photo/VZKz-hJ23kmr5ebsAh3zGViAXUOsgO3ZabYSC4FW3KZNp6aAmzSAFOoDMLvPxSphWAoamrfHZeewC5uTCE3oryTH8sQn.jpg',
                    'https://s1.auto.drom.ru/photo/zAt16u_WTgywrSV7mEEwbaRsdVj3hspF4mfrEqVkFE2AdoSFB-bLtXJL2jKyutnZbbZvSv7KNfwkwN-5823LnVZ6TzFR.jpg',
                    'https://s1.auto.drom.ru/photo/-mTwsaj9SR21t5ZDzs3wFyW-1iOUWtkeNW-90YQV8hxinozmCCC5-QM-BAH8uC8a-n5mCZjk-RYClCN_QLL90pbOWjPB.jpg',
                    'https://s1.auto.drom.ru/photo/kFgVF9FTxsPTKO8Coe1zkf7HSJXNSXExISNlc3aTxMRmH88A410EPnt92D5VH6da-mJ3q35MiXdfD0IkPWHxaQfC5E7t.jpg',
                    'https://s1.auto.drom.ru/photo/3hmEEZCSYw-9D3yWlQvtTI0NMX6fwmSba695um9o9g-QOgwTj-ZP79Ibxma0VJjILDWJq030to3AlqZZtMInqHnHHcNF.jpg',
                    'https://s1.auto.drom.ru/photo/bNSsGQXb2nMUXzc8N_UUah8G-PXRPHWeL7tgZp7-eQ68JHS1uRE8YALoalVyka3QyiGVLqu3mv6KJv8Mh-2p6XiCcGvd.jpg',
                    'https://s1.auto.drom.ru/photo/2wCEts-Y50JcDvebFPyCUuhiqn7FJSJIl2_IyDxr4pKba5CP-5euqV4fTfqAc-GGaU-XyUpceoU1WNRpPw_JOFY3nZZb.jpg',
                    'https://s1.auto.drom.ru/photo/K0FfT7DpqKL92MH2FeObmwmI24k8YoW4NjsJKeCt-Ga4xuwhYvtOEiUD7T6OHGvA77UexF_9PXW0zsoeAUBvjQlNAIRt.jpg',
                    'https://s1.auto.drom.ru/photo/pds8x0mYHx0I_IVNNeJhpL8Ml70QQpnNmChD0xN-jMHGkocAVzw5H6SfWXlUiE7UCEG7zn6jjkkZhE0DqPIv3Z-y29JY.jpg',
                    'https://s1.auto.drom.ru/photo/FNtW5VenVsRNehsBeupGO9RFUNBkJcrydVHwBrU99Nm6U8DwkbNi4NLmxIjRXsjcy2P5FDbGTfDoAyD-q_CQmSbfZh8v.jpg',
                    'https://s1.auto.drom.ru/photo/-L2XrFJWRbTjKJXa2F_FrkZ7FTWrASa1pdLFLBUEOFuOHjKEsaMcLygINjDoF_gqHcSSVWv_x7_I0pyjMBalSRyDToxe.jpg',
                    'https://s1.auto.drom.ru/photo/JM6m_HvC9tRviKWLkeYrCJbQ_8ruZs0PsBRMgEuaxZKliSDlM5JvAFSBEwduVQlRzja5bYdx6ijXILFT7j0A1oMe6sFH.jpg',
                    'https://s1.auto.drom.ru/photo/Wm_1-jC2ziOnlVcU1shV3ekYMH2wwFPt8B-zK0qfywgD5-VAneW-cTAoiUXZBT33pZuxxvGAEA_dlBEPPPwY34fnrL1i.jpg',
                    'https://s1.auto.drom.ru/photo/4b0-Th147FOGMh6lylAVrEzpFMucPPm7tiQ0Uo2HD_eWJhG2R-iN-nS7KDz7CrmdtcbDk_6HGefBXQ3OfkPaH_uvUGUd.jpg',
                    'https://s1.auto.drom.ru/photo/E3K6mJCY79Fe4POkKc1b2Ne3xYbFokTRyhp0pbsxma-LNtNmUkyuDUnoFJyFZfWBRYsYnjp2YdWyLCpgERekEv0xJe65.jpg',
                    'https://s1.auto.drom.ru/photo/0JrzzsBNEr--5-gRZWcb-GmjDJAW-m_MuGSxIvNc6mbSPsJxnhImemG1ltP1oqW-1v37O_u387PvJHkoTbllkRNLU1OJ.jpg',
                    'https://s1.auto.drom.ru/photo/uRrjGeR8As7EPDEzHMwekujVeri-YrQjotN-VfTXB71rzn5tZLLnlR5D3TUuD3vxLVSamPz88PMxh7eO6kpG-PbikxJP.jpg',
                    'https://s1.auto.drom.ru/photo/U-cXwc9faaVktsjj0YiloZdOvu3QQipdc5-7c7Y8ZvJZseW6FX0lFvZ1Gzn0hbwsgTISSp_RO8tLmGdDZN-vbdrTExfc.jpg',
                    'https://s1.auto.drom.ru/photo/fwMwElMkbkMKHcrFGk7m5Pn67eI51wVMYyTyIFNuvT-XZyahZB8gh5HWFZrIyTLNUEIt5Tazg_ieBaprDw4znSH24tQP.jpg',
                    'https://s1.auto.drom.ru/photo/mhzek98Jep2EC9MV-u9GK0xw_PjtjaNZKWyZ-kqcwlVB-X5J6V18R6L3vz_lEoz5iJ7KrOunOFjwSu06FhxTuZ3pk25a.jpg',
                    'https://s1.auto.drom.ru/photo/7qIPOxK1scvcjHSYDOUFLPEFZokHWTAuyhANRPDwZLvnh297snjVaOMBRbWp1pCUBhW3vqFd7QtyVKklrqojjTkHRRRC.jpg',
                    'https://s1.auto.drom.ru/photo/MrL1iRU9pS57C10hsLrRQS07AtNKg-DE6_8AXua_nSSiWq3QjNkXzf1MTUryBAUrkyEGjPGVBAbcMT8I0weef8Tpn-Tf.jpg',
                ]),
            (new ResultAuto(51718410, 'https://alushta.drom.ru/subaru/legacy/51718410.html'))
                ->setBrand('Subaru')
                ->setModel('Legacy')
                ->setPrice(550000)
                ->setDromPrice(425000)
                ->setDealType('высокая цена')
                ->setGeneration('3 поколение')
                ->setComplectation('2.5 AT GX')
                ->setMileage('400000 км')
                ->setColor('белый')
                ->setFrameType('Седан')
                ->setPower('156 л.с.')
                ->setFuel('бензин')
                ->setVolume('2.5 л')
                ->setPhotoUrls([
                    'https://s1.auto.drom.ru/photo/0Tm24xY-uIraRZngFDHabekKeJcHd8Y2Nwnk5EPVIWiJ9fgZ-w_1udtSVuOsBQadb4rBNxKSY66PzphB8j-BiteNaubq.jpg',
                    'https://s1.auto.drom.ru/photo/1moMvzA1mtuife2mMGZB1rfhRqwrfqJAmHA_09p2-o5K7Xk-zmf-IEazsqOMNPyFyarzGftiZdPaH9xcVxD_DaVwIy-o.jpg',
                    'https://s1.auto.drom.ru/photo/EiR5PAa5knLMKMR5GR7OH3g56AsZLBj2z-1q9-nVQG3M6IuwV2ak3EQWa0fu6BIZHSebF6EXwqBrkqmkCVRjBDnQ5vrH.jpg',
                    'https://s1.auto.drom.ru/photo/E9TiJNUXCfDIWljrcAS-1ZevT6UjBkqUwQRhMKgiBIZbxU6TqgF9ZXe5Xjx8BShY2na6Nfv7ZOKXGCQ9xa5UQLR5CVqI.jpg',
                    'https://s1.auto.drom.ru/photo/y98QSqxCry-xcs7lsaJ4sCJMxgBf_8BFv_2vOBfczQTv93M55VZaqNDBJ7zJ2xRpxOBFH-g6__csUP_NE1oRdEuhSmLI.jpg',
                    'https://s1.auto.drom.ru/photo/N8Y8w5JbI8d9DuMXG9_BF-EV59RkMEI4Vu3hR7iSSYuxgIMFbKbyps1o_1MDYRKttr4TKpYKdesBZug-Iai73ZXt8mz6.jpg',
                    'https://s1.auto.drom.ru/photo/oEDW3R65Xe6XSperrdFooBxv7-lqNMG095sbECD60rE3HNsuaIRnPSTkpibD1LUPFDIgz-E8r6056tKE3GzBRV249PN_.jpg',
                    'https://s1.auto.drom.ru/photo/BIk2-Fwclaefm-RcgOt5M3788fbBkO6yvFjeJaSjHXzpi8EGg12_skhKn6hOOZw_3fmB4UdPNHoUAWDmBiOHcyuL69Oz.jpg',
                    'https://s1.auto.drom.ru/photo/WptFX7bRcUkubeMVizCEJRxmTSIiE3EKH2zBlH3ITsaq0PkQTJ0C8HBOPNw0xlAmhQ3_Jqp4GJNhQJH2ALakrcupLDr8.jpg',
                    'https://s1.auto.drom.ru/photo/e7byMXCsotpbAT_xH72qwhHXB3JcSiGwInnAjrCbdeCzpGFZ48X1x3oc8k3U9BYH_ZpAnjaTBfbFfc94XJfe6MOk3n44.jpg',
                    'https://s1.auto.drom.ru/photo/RC2AzJpVLlZ-RtnnDUHtOanPUKdyT1bLRf-Al10sBG-HUi3EGdV1obdJq8KyKminOjPDurQCkaixa8Se5_oYfIQkXeGw.jpg',
                    'https://s1.auto.drom.ru/photo/o97zUO2HzNnO2fzLG3IY8i_y3tXkyq5BqEILlKPbDxCKsPm4SuhzzgEl-hWE6cHAIA-C5mTP1v9nJFmVqzGKb7MqW73N.jpg',
                    'https://s1.auto.drom.ru/photo/ymbqfls-ScjOOiHRWLYRP7oykri8kak-kbWBDv1o4OvprPgbC3Lku6-wIRph9Wbs8cxFItBErFkgtQtJUXFvQxbMOnYy.jpg',
                    'https://s1.auto.drom.ru/photo/jeCJFE2fV1gGKBgkN6jxtF8tKQDndPANw7uC7afmz9FiF_LHYyeeNiP_Jf98_4PJtxqTvFrI9xJHZ2kgxMrLMYAuf0CI.jpg',
                    'https://s1.auto.drom.ru/photo/QLGvdpmuUYcJJOgOOqbVgpF_9tWoucLtgGV8VHflEJXIvZueqft7qA2IqEGdTiGowf5ODTeIsksRLjNAaePpT0vEcue1.jpg',
                ]),
        ];
    }
}
