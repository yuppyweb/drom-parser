<?php declare(strict_types=1);

namespace DromParser\Test\Result;

use DromParser\Result\ResultAuto;
use PHPUnit\Framework\TestCase;

class ResultAutoTest extends TestCase
{
    /**
     * @dataProvider toArrayDataProvider
     */
    public function testToArray(array $params, array $expectedResult): void
    {
        $resultAuto = new ResultAuto($params['dromId'], $params['url']);

        if (!empty($params['brand'])) {
            $resultAuto->setBrand($params['brand']);
        }
        
        if (!empty($params['model'])) {
            $resultAuto->setModel($params['model']);
        }
        
        if (!empty($params['price'])) {
            $resultAuto->setPrice($params['price']);
        }
        
        if (!empty($params['dromPrice'])) {
            $resultAuto->setDromPrice($params['dromPrice']);
        }
        
        if (!empty($params['dealType'])) {
            $resultAuto->setDealType($params['dealType']);
        }
        
        if (!empty($params['generation'])) {
            $resultAuto->setGeneration($params['generation']);
        }
        
        if (!empty($params['complectation'])) {
            $resultAuto->setComplectation($params['complectation']);
        }
        
        if (!empty($params['mileage'])) {
            $resultAuto->setMileage($params['mileage']);
        }
        
        if (!empty($params['mileageInRussia'])) {
            $resultAuto->setMileageInRussia($params['mileageInRussia']);
        }
        
        if (!empty($params['color'])) {
            $resultAuto->setColor($params['color']);
        }
        
        if (!empty($params['frameType'])) {
            $resultAuto->setFrameType($params['frameType']);
        }
        
        if (!empty($params['power'])) {
            $resultAuto->setPower($params['power']);
        }
        
        if (!empty($params['fuel'])) {
            $resultAuto->setFuel($params['fuel']);
        }
        
        if (!empty($params['volume'])) {
            $resultAuto->setVolume($params['volume']);
        }
        
        if (!empty($params['isHybrid'])) {
            $resultAuto->setIsHybrid($params['isHybrid']);
        }
        
        if (!empty($params['isGbo'])) {
            $resultAuto->setIsGbo($params['isGbo']);
        }
        
        if (!empty($params['photoUrls'])) {
            $resultAuto->setPhotoUrls($params['photoUrls']);
        }

        $this->assertEquals($expectedResult, $resultAuto->toArray());
    }
    
    public static function toArrayDataProvider(): array
    {
        return [
            [
                [
                    'dromId' => 52088341,
                    'url' => 'https://novosibirsk.drom.ru/subaru/legacy/52088341.html',
                ],
                [
                    'dromId' => 52088341,
                    'url' => 'https://novosibirsk.drom.ru/subaru/legacy/52088341.html',
                    'brand' => null,
                    'model' => null,
                    'price' => null,
                    'dromPrice' => null,
                    'dealType' => null,
                    'generation' => null,
                    'complectation' => null,
                    'mileage' => null,
                    'mileageInRussia' => null,
                    'color' => null,
                    'frameType' => null,
                    'power' => null,
                    'fuel' => null,
                    'volume' => null,
                    'isHybrid' => null,
                    'isGbo' => null,
                    'photoUrls' => null,
                ],
            ],
            [
                [
                    'dromId' => 52088341,
                    'url' => 'https://novosibirsk.drom.ru/subaru/legacy/52088341.html',
                    'brand' => 'Subaru',
                    'model' => 'Legacy',
                    'price' => 500000,
                    'dromPrice' => 395000,
                    'dealType' => 'высокая цена',
                    'generation' => '3 поколение',
                    'complectation' => 'GX AWD',
                    'mileage' => '260000 км',
                    'mileageInRussia' => 'без пробега по РФ',
                    'color' => 'серебристый',
                    'frameType' => 'Седан',
                    'power' => '220 л.с.',
                    'fuel' => 'бензин',
                    'volume' => '3 л',
                    'isHybrid' => 'гибрид',
                    'isGbo' => 'ГБО',
                    'photoUrls' => [
                        'https://s1.auto.drom.ru/photo/g5O_EopbU79b6KG3-njncVJ6alXZmPmF1mAo3qIttq6xbwNp3zm8nvm8Na97hpo0XKyuwCho91lk65kWWFh3HLPT0Sob.jpg',
                        'https://s1.auto.drom.ru/photo/4qPgexGD6H-CboigLwhBhqelEeGxHAVNVpNfRJkRzH1OB3nHJTuNJIVq-cmGRg-j3nZHm3A_jeVRZ8tCFaqCZjzz7EMf.jpg',
                        'https://s1.auto.drom.ru/photo/E5awpv3mQvgLSNo733tECEIBdQl9nCykXvkNFGimVTkLLuTelzW0qdSAmABa6aqR9A4lVOhoL4LbXEp5xTnrK9xzljTf.jpg',
                    ],
                ],
                [
                    'dromId' => 52088341,
                    'url' => 'https://novosibirsk.drom.ru/subaru/legacy/52088341.html',
                    'brand' => 'Subaru',
                    'model' => 'Legacy',
                    'price' => 500000,
                    'dromPrice' => 395000,
                    'dealType' => 'высокая цена',
                    'generation' => '3 поколение',
                    'complectation' => 'GX AWD',
                    'mileage' => '260000 км',
                    'mileageInRussia' => 'без пробега по РФ',
                    'color' => 'серебристый',
                    'frameType' => 'Седан',
                    'power' => '220 л.с.',
                    'fuel' => 'бензин',
                    'volume' => '3 л',
                    'isHybrid' => 'гибрид',
                    'isGbo' => 'ГБО',
                    'photoUrls' => [
                        'https://s1.auto.drom.ru/photo/g5O_EopbU79b6KG3-njncVJ6alXZmPmF1mAo3qIttq6xbwNp3zm8nvm8Na97hpo0XKyuwCho91lk65kWWFh3HLPT0Sob.jpg',
                        'https://s1.auto.drom.ru/photo/4qPgexGD6H-CboigLwhBhqelEeGxHAVNVpNfRJkRzH1OB3nHJTuNJIVq-cmGRg-j3nZHm3A_jeVRZ8tCFaqCZjzz7EMf.jpg',
                        'https://s1.auto.drom.ru/photo/E5awpv3mQvgLSNo733tECEIBdQl9nCykXvkNFGimVTkLLuTelzW0qdSAmABa6aqR9A4lVOhoL4LbXEp5xTnrK9xzljTf.jpg',
                    ],
                ],
            ],
        ];
    }
}
